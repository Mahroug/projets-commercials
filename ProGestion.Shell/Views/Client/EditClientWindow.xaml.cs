﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Xpf.Core;
using ProGestion.Data.Bll;
using ProGestion.Data.Layers.Entities;
using ProGestion.Shell.Views.CategoriePersonne;


namespace ProGestion.Shell.Views.Client
{
    /// <summary>
    /// Logique d'interaction pour EditClientWindow.xaml
    /// </summary>
    public partial class EditClientWindow
    {
        public Data.Layers.Entities.Personne Client = new Data.Layers.Entities.Personne();
        public EditClientWindow(Data.Layers.Entities.Personne client)
        {
            InitializeComponent();
            this.Client = client;
            FillData();
        }
        private void EditClientWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            SexeComboBox.ItemsSource = new string[2] { "Masculin", "Féminin" };
            CategoriePersonne.ItemsSource = SharedBll.Db.CategoriePersonnes.Local.ToArray();
        }

        private void FillData()
        {
            if (Client == null)Close();
            
            if (Client.Sexe == 'm')
                SexeComboBox.SelectedItem = "Masculin";
            else if (Client.Sexe == 'f')
                SexeComboBox.SelectedItem = "Féminin";

            CategoriePersonne.SelectedItem = Client.CategoriePersonne;
            Nom.Text = Client.Nom;
            Prenom.Text = Client.Prenom;
            Addresse.Text = Client.Adresse;
            Email.Text = Client.Email;
            Mobile.Text = Client.Mobile;
            Telephone.Text = Client.Telephone;
        }

        private void SaveClick(object sender, RoutedEventArgs e)
        {
            if (!VerifyFieldsFields()) return;
            EditClient.IsEnabled = false;
            try
            {
                var sexe = SexeComboBox?.SelectedItem as string;
                var sexeDesignation = new Char();
                if (sexe == "Masculin")
                    sexeDesignation = 'm';
                else if (sexe == "Féminin")
                    sexeDesignation = 'f';

                Client.CategoriePersonne = CategoriePersonne.SelectedItem as Data.Layers.Entities.CategoriePersonne;
                Client.Nom = Nom?.Text;
                Client.Prenom = Prenom?.Text;
                Client.Sexe = sexe != null ? sexeDesignation : new Char();
                Client.Adresse = Addresse?.Text;
                Client.Email = Email?.Text;
                Client.Mobile = Mobile?.Text;
                Client.Telephone = Telephone?.Text;
                Client.TypePersonne = SharedBll.Db.TypePersonnes.FirstOrDefault(p => p.Id == 1);
                Client.DateCreation = DateTime.Now;
                Client.CreePar = SharedBll.CurrentUser;

                PersonneBll.UpdatePersonne(Client);
                SharedBll.Db.SaveChanges();

                EditClient.IsEnabled = true;
                Close();
                DXMessageBox.Show("La modification a été fait avec succès", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Phone_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex _regex = new Regex("[^0-9]+");
            e.Handled = _regex.IsMatch(e.Text);
        }

        private void AddCategorieClick(object sender, RoutedEventArgs e)
        {
            var newCategorie = new AddCategoriePersonneWindow();
            newCategorie.ShowDialog();
            EditClientWindow_OnLoaded(null, null);
        }

        private bool VerifyFieldsFields()
        {

            if (Nom.Text == "")
            {
                Nom.Focus();
                return false;
            }

            if (Prenom.Text == "")
            {
                Prenom.Focus();
                return false;
            }

            return true;
        }
    }
}
