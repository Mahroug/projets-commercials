﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DevExpress.Xpf.Core;
using ProGestion.Data.Bll;
using Exception = System.Exception;

namespace ProGestion.Shell.Views.Client
{
    /// <summary>
    /// Logique d'interaction pour ClientUc.xaml
    /// </summary>
    public partial class ClientUc : UserControl
    {
        private ObservableCollection<Data.Layers.Entities.Personne> _manageSource = new ObservableCollection<Data.Layers.Entities.Personne>();
        public ClientUc()
        {
            InitializeComponent();
            Refresh();
        }

        private void AddPersonneClick(object sender, RoutedEventArgs e)
        {
            new AddClientWindow().ShowDialog();
            Refresh();
        }

        private void EditPersonneClick(object sender, RoutedEventArgs e)
        {
            new EditClientWindow(ManagGridControl.SelectedItem as Data.Layers.Entities.Personne).ShowDialog();
            Refresh();
        }

        private void DeletePersonneClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var x = DXMessageBox.Show("voulez-vous supprimé cette Personne?", "info", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (x == MessageBoxResult.Yes)
                {
                     var b =PersonneBll.RemovePersonne(ManagGridControl.SelectedItem as Data.Layers.Entities.Personne);
                    if (b==false)
                        DXMessageBox.Show("Vous ne pouvez pas supprimé ce client car il est déja utilisé", "info", MessageBoxButton.OK, MessageBoxImage.Information);
                    SharedBll.Db.SaveChanges();
                    Refresh();
                }
            }

            catch (Exception ex)
            {
                DXMessageBox.Show(ex.Message, "error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void SearchBox_OnKeyUp(object sender, KeyEventArgs e)
        {
            try {
                ManagGridControl.ItemsSource = null;
            
           
                ManagGridControl.ItemsSource = SharedBll.Db.Personnes.Local.Where(c => (c.TypePersonne?.Id == 1) && (c.Nom.ToLower().Contains(SearchBox.Text.ToLower()) ||
                                                                                        c.Mobile.ToLower().Contains(SearchBox.Text.ToLower()) ||
                                                                                        c.Nom.ToLower().Contains(SearchBox.Text.ToLower()) ||
                                                                                        c.Prenom.ToLower().Contains(SearchBox.Text.ToLower())));
            }
            catch (Exception exception)
            {

            }
}

        private void MenuItemCopy_OnClick(object sender, RoutedEventArgs e)
        {
            object cellValue = null;
            if (ManagTableView?.FocusedColumn != null)
            {
                cellValue = ManagGridControl.GetFocusedRowCellValue(ManagTableView?.FocusedColumn);
            }
            if (cellValue != null)
                Clipboard.SetText(cellValue.ToString());
        }

        private void Refresh()
        {
            try
            {
                _manageSource = null;
                _manageSource = new ObservableCollection<Data.Layers.Entities.Personne>(SharedBll.Db.Personnes.Local.Where(c => c.TypePersonne?.Id == 1));

                ManagGridControl.ItemsSource = _manageSource;
            }

            catch (Exception ex)
            {
                DXMessageBox.Show(ex.Message, "error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
