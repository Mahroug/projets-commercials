﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;
using DevExpress.Xpf.Core;
using ProGestion.Data.Bll;
using ProGestion.Shell.Views.CategoriePersonne;

namespace ProGestion.Shell.Views.Client
{
    /// <summary>
    /// Logique d'interaction pour AddClientWindow.xaml
    /// </summary>
    public partial class AddClientWindow
    {
        public AddClientWindow()
        {
            InitializeComponent();
        }

        private bool VerifyFieldsFields()
        {
            if (Nom.Text == "")
            {
                Nom.Focus();
                return false;
            }

            if (Prenom.Text == "")
            {
                Prenom.Focus();
                return false;
            }

            return true;
        }
        private void SaveClick(object sender, RoutedEventArgs e)
        {
            if (!VerifyFieldsFields()) return;
            try
            {
                var categoriePersonne = CategoriePersonne?.SelectedItem as Data.Layers.Entities.CategoriePersonne;
                var sexe = SexeComboBox?.SelectedItem as string;
                var sexeDesignation = new Char();
                if (sexe != null && sexe == "Masculin")
                    sexeDesignation = 'm';
                else if (sexe != null && sexe == "Féminin")
                    sexeDesignation = 'f';
                var client = new Data.Layers.Entities.Personne()
                {
                    Nom = Nom?.Text,
                    Prenom = Prenom?.Text,
                    Sexe = sexe != null ? sexeDesignation : new Char(),
                    CategoriePersonne = categoriePersonne,
                    Adresse = Addresse?.Text,
                    Email = Email?.Text,
                    Mobile = Mobile?.Text,
                    Telephone = Telephone?.Text,
                    TypePersonne = SharedBll.Db.TypePersonnes.Local.FirstOrDefault(p => p.Id == 1),
                    DateCreation = DateTime.Now,
                    CreePar = SharedBll.CurrentUser,
                };
                var found = PersonneBll.Found(client);
                if (!found)
                {
                    PersonneBll.AddPersonne(client);

                    SharedBll.Db.SaveChanges();
                    DXMessageBox.Show("L'ajout a été fait avec succès", "info", MessageBoxButton.OK, MessageBoxImage.Information);
                    Close();
                }
                else
                {
                    DXMessageBox.Show("Le client existe déja", "info", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Phone_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex _regex = new Regex("[^0-9]+");
            e.Handled = _regex.IsMatch(e.Text);
        }

        private void AddWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            SexeComboBox.ItemsSource = new string[2] { "Masculin", "Féminin" };
            CategoriePersonne.ItemsSource = SharedBll.Db.CategoriePersonnes.Local.ToArray();
        }

        private void AddCategorieClick(object sender, RoutedEventArgs e)
        {
            var newCategorie = new AddCategoriePersonneWindow();
            newCategorie.ShowDialog();
            AddWindow_OnLoaded(null, null);
        }

    }
}
