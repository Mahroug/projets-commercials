﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Xpf.Core;
using ProGestion.Data.Bll;
using ProGestion.Data.Layers.Entities;

namespace ProGestion.Shell.Views.Marque
{
    /// <summary>
    /// Logique d'interaction pour EditMarqueWindow.xaml
    /// </summary>
    public partial class EditMarqueWindow
    {
        public Data.Layers.Entities.Marque Marque = new Data.Layers.Entities.Marque();
        public EditMarqueWindow(Data.Layers.Entities.Marque marque)
        {
            InitializeComponent();
            this.Marque = marque;
            FillData();
        }

        private void FillData()
        {
            Designation.Text = Marque.Designation.ToString();
        }

        private void SaveClick(object sender, RoutedEventArgs e)
        {
            if (!VerifyFieldsFields()) return;
            EditMarque.IsEnabled = false;
            try
            {

                Marque.Designation = Designation.Text;

                MarqueBll.UpdateMarque(Marque);
                SharedBll.Db.SaveChanges();

                EditMarque.IsEnabled = true;
                Close();
                DXMessageBox.Show("La modification a été éffectuée avec succès", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private bool VerifyFieldsFields()
        {

            if (Designation.Text == "")
            {
                Designation.Focus();
                return false;
            }

            return true;
        }
    }
}
