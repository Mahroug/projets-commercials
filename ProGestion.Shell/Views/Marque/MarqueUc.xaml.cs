﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.Xpf.Core;
using ProGestion.Data.Bll;
using ProGestion.Shell.Views.Marque;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using UserControl = System.Windows.Controls.UserControl;

namespace ProGestion.Shell.Views.Marque
{
    /// <summary>
    /// Logique d'interaction pour Marque.xaml
    /// </summary>
    public partial class MarqueUc
    {
        private ObservableCollection<Data.Layers.Entities.Marque> _manageSource =
            new ObservableCollection<Data.Layers.Entities.Marque>();
        public MarqueUc()
        {
            InitializeComponent();
            Refresh();
            ManagTableView.BestFitColumns();
            ManagGridControl.TotalSummary.Add(SummaryItemType.Count, "Id");
        }

        private void Refresh()
        {
            //change font size
            ManagGridControl.FontSize = SharedBll.UcFontSize;

            _manageSource = null;
            _manageSource = new ObservableCollection<Data.Layers.Entities.Marque>(SharedBll.Db.Marques);

            if (!_manageSource.Any()) return;


            try
            {

                ManagGridControl.ItemsSource = null;
                ManagTableView.RowMinHeight = SharedBll.UcFontSize * 2.85;
                Search(true);

            }
            catch (Exception e)
            {
                DXMessageBox.Show(e.Message, "error", MessageBoxButton.OK, MessageBoxImage.Error);

            }
        }

        private void Search(bool keyUpEmptyText)
        {
            try
            {
                ManagGridControl.ItemsSource = null;

                if (SearchBox.Text.Trim() == String.Empty && keyUpEmptyText)
                {
                    ManagGridControl.ItemsSource =
                        new ObservableCollection<Data.Layers.Entities.Marque>(SharedBll.Db.Marques);
                    return;
                }

                ManagGridControl.ItemsSource = null;
                ManagGridControl.ItemsSource = _manageSource.Where(c =>
                    (c.Designation != null &&
                     c.Designation.ToLower().Contains(SearchBox.Text.ToLower()))
                    );
            }
            catch (Exception exception)
            {

            }
        }
        private void Excel_OnClick(object sender, RoutedEventArgs e)
        {

            try
            {
                var saveFileDialog = new SaveFileDialog
                {
                    InitialDirectory = Convert.ToString(Environment.SpecialFolder.Desktop),
                    Filter = @"(*.XLSX)|*.xlsx|All Files (*.*)|*.*",
                    FilterIndex = 1,
                    FileName = "List"
                };

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                    ManagTableView.ExportToXlsx(saveFileDialog.FileName);
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);

            }

        }


        private void DeleteMarqueClick(object sender, RoutedEventArgs e)
        {
            if (ManagGridControl.SelectedItem == null) return;

            Data.Layers.Entities.Marque marque;
            try
            {
                marque = ManagGridControl.SelectedItem as Data.Layers.Entities.Marque;
                var result = DXMessageBox.Show("Voulez vous supprimer cette ligne?", "Info", MessageBoxButton.YesNo,
                    MessageBoxImage.Warning);
                if (result != MessageBoxResult.Yes) return;

                MarqueBll.RemoveCategoryPersonne(marque);
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            SharedBll.Db.SaveChanges();
            DXMessageBox.Show("suppression avec succès", "info", MessageBoxButton.OK, MessageBoxImage.Information);
            Search(true);
        }

        private void EditMarqueClick(object sender, RoutedEventArgs e)
        {
            var marque = ManagGridControl.SelectedItem as Data.Layers.Entities.Marque;
            if (marque == null) return;

            new EditMarqueWindow(marque).ShowDialog();
            Refresh();
        }

        private void AddMarqueClick(object sender, RoutedEventArgs e)
        {
            new AddMarqueWindow().ShowDialog();
            Refresh();
        }

        private void SearchBox_OnKeyUp(object sender, KeyEventArgs e)
        {
            Search(SearchBox.Text.Trim() != String.Empty);
        }
    }
}
