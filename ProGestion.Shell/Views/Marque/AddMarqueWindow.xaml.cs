﻿using System;
using System.Windows;
using DevExpress.Xpf.Core;
using ProGestion.Data.Bll;

namespace ProGestion.Shell.Views.Marque
{
    public partial class AddMarqueWindow
    {
        public AddMarqueWindow()
        {
            InitializeComponent();
        }

        private void SaveClick(object sender, RoutedEventArgs e)
        {
            ///// verify fields
            if (!VerifyFieldsFields()) return;
            AddMarque.IsEnabled = false;
            try
            {
                var marque = new Data.Layers.Entities.Marque
                {
                    Designation = Designation.Text,
                };
                MarqueBll.AddMarque(marque);
                SharedBll.Db.SaveChanges();

                AddMarque.IsEnabled = true;
                Close();
                DXMessageBox.Show("L'ajout a été éffecuté avec succès", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }
        private bool VerifyFieldsFields()
        {

            if (Designation.Text == "")
            {
                Designation.Focus();
                return false;
            }

            return true;
        }
    }
}
