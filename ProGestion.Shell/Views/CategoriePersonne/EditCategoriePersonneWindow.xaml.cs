﻿using System;
using System.Windows;
using System.Windows.Media;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Editors.Helpers;
using ProGestion.Data.Bll;

namespace ProGestion.Shell.Views.CategoriePersonne
{
    public partial class EditCategoriePersonneWindow
    {
        private Data.Layers.Entities.CategoriePersonne _categoriePersonne;

        public EditCategoriePersonneWindow(Data.Layers.Entities.CategoriePersonne categoriePersonne)
        {
            InitializeComponent();
            _categoriePersonne = categoriePersonne;
            FillData();
        }

        private void FillData()
        {
            Designation.Text = _categoriePersonne.Designation;
            BackgroundColor.Background = (Brush)new BrushConverter().ConvertFrom(_categoriePersonne.Couleur);
        }

        private void SaveClick(object sender, RoutedEventArgs e)
        {

            if (Designation.Text != _categoriePersonne.Designation || BackgroundColor.Background.ToString() != _categoriePersonne.Couleur)
            {
                if (Designation.Text == "")
                {
                    Designation.Focus();
                    return;
                }
                try
                {
                    _categoriePersonne.Designation = Designation.Text;
                    _categoriePersonne.Couleur = BackgroundColor.Background.ToString();

                    CategoriePersonneBll.UpdateCategoriePersonne(_categoriePersonne);

                    SharedBll.Db.SaveChanges();

                    Close();
                    DXMessageBox.Show("La modification a été éffectuée avec succeés", "info", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                catch (Exception exception)
                {
                    DXMessageBox.Show(exception.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                Close();
            }
        }

        private void BackgroundColor_OnClick(object sender, RoutedEventArgs e)
        {
            if (BackgroundColorStackPanel.Visibility == Visibility.Collapsed)
            {
                BackgroundColorStackPanel.Visibility = Visibility.Visible;
                Save.IsEnabled = false;
                Designation.IsEnabled = false;
                BackgroundCancelBtn.Visibility = Visibility.Visible;
                BackgroundColorBtn.Content = "";
                return;
            }

            BackgroundColorStackPanel.Visibility = Visibility.Collapsed;
            BackgroundColor.Background = BackgroundColorChooser.Color.ToSolidColorBrush();
            BackgroundColorBtn.Content = "";
            Save.IsEnabled = true;
            Designation.IsEnabled = true;
            BackgroundCancelBtn.Visibility = Visibility.Collapsed;
        }

        private void BackgroundCancel_OnClick(object sender, RoutedEventArgs e)
        {
            BackgroundColorStackPanel.Visibility = Visibility.Collapsed;
            BackgroundColorBtn.Content = "";
            Save.IsEnabled = true;
            Designation.IsEnabled = true;
            BackgroundCancelBtn.Visibility = Visibility.Collapsed;
        }

    }
}
