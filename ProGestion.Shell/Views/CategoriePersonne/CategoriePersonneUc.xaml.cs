﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.Xpf.Core;
using ProGestion.Data.Bll;
using ProGestion.Shell.Views.CategoriePersonne;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using UserControl = System.Windows.Controls.UserControl;

namespace ProGestion.Shell.Views.CategoriePersonne
{
    /// <summary>
    /// Logique d'interaction pour CategoriePersonne.xaml
    /// </summary>
    public partial class CategoriePersonneUc
    {
        private ObservableCollection<Data.Layers.Entities.CategoriePersonne> _manageSource =
            new ObservableCollection<Data.Layers.Entities.CategoriePersonne>();
        public CategoriePersonneUc()
        {
            InitializeComponent();
            Refresh();
            ManagTableView.BestFitColumns();
            ManagGridControl.TotalSummary.Add(SummaryItemType.Count, "Id");
        }

        private void Refresh()
        {
            ManagGridControl.FontSize = SharedBll.UcFontSize;

            _manageSource = null;
            _manageSource = new ObservableCollection<Data.Layers.Entities.CategoriePersonne>(SharedBll.Db.CategoriePersonnes);

            if (!_manageSource.Any()) return;
            try
            {
                ManagGridControl.ItemsSource = null;
                ManagTableView.RowMinHeight = SharedBll.UcFontSize * 2.85;
                Search(true);
            }
            catch (Exception e)
            {
                DXMessageBox.Show(e.Message, "error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Search(bool keyUpEmptyText)
        {
            ManagGridControl.ItemsSource = null;

            try
            {
                if (SearchBox.Text.Trim() == String.Empty && keyUpEmptyText)
                {
                    ManagGridControl.ItemsSource =
                        new ObservableCollection<Data.Layers.Entities.CategoriePersonne>(SharedBll.Db.CategoriePersonnes);
                    return;
                }

                ManagGridControl.ItemsSource = null;
                ManagGridControl.ItemsSource = _manageSource.Where(c =>
                    (c.Designation != null &&
                     c.Designation.ToLower().Contains(SearchBox.Text.ToLower()))
                );
            }
            catch (Exception e)
            {
                DXMessageBox.Show(e.Message, "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void Excel_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var saveFileDialog = new SaveFileDialog
                {
                    InitialDirectory = Convert.ToString(Environment.SpecialFolder.Desktop),
                    Filter = @"(*.XLSX)|*.xlsx|All Files (*.*)|*.*",
                    FilterIndex = 1,
                    FileName = "List"
                };

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                    ManagTableView.ExportToXlsx(saveFileDialog.FileName);
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }


        private void DeleteCategoriePersonneClick(object sender, RoutedEventArgs e)
        {
            if (ManagGridControl.SelectedItem == null) return;

            try
            {
                var categoriePersonne = ManagGridControl.SelectedItem as Data.Layers.Entities.CategoriePersonne;
                var result = DXMessageBox.Show("Voulez vous supprimer cette ligne?", "Info", MessageBoxButton.YesNo,
                    MessageBoxImage.Warning);
                if (result != MessageBoxResult.Yes) return;

               var b= CategoriePersonneBll.RemoveCategoryPersonne(categoriePersonne);
                if (b == false)
                {
                    DXMessageBox.Show("Vous ne peuvez pas supprimer cette categorie car elle est déja utilisé", "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            SharedBll.Db.SaveChanges();
            DXMessageBox.Show("suppression avec succès", "info", MessageBoxButton.OK, MessageBoxImage.Information);
            Search(true);
        }

        private void EditCategoriePersonneClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var category = ManagGridControl.SelectedItem as Data.Layers.Entities.CategoriePersonne;
                if (category == null) return;

                new EditCategoriePersonneWindow(category).ShowDialog();
                Refresh();
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddCategoriePersonneClick(object sender, RoutedEventArgs e)
        {
            new AddCategoriePersonneWindow().ShowDialog();
            Refresh();
        }

        private void SearchBox_OnKeyUp(object sender, KeyEventArgs e)
        {
            Search(SearchBox.Text.Trim() != String.Empty);
        }
    }
}
