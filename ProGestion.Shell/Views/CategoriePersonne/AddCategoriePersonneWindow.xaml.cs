﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Editors.Helpers;
using ProGestion.Data.Bll;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;

namespace ProGestion.Shell.Views.CategoriePersonne
{
    /// <summary>
    /// Interaction logic for AddCategoriePersonneWindow.xaml
    /// </summary>
    public partial class AddCategoriePersonneWindow
    {
        public Data.Layers.Entities.CategoriePersonne CategoriePersonne;

        public AddCategoriePersonneWindow()
        {
            InitializeComponent();
        }

        private void SaveClick(object sender, RoutedEventArgs e)
        {
            if (Designation.Text == "")
            {
                Designation.Focus();
                return;
            }

            try
            {
                CategoriePersonne = new Data.Layers.Entities.CategoriePersonne
                {
                    Designation = Designation.Text,
                    Couleur = BackgroundColor.Background.ToString()
                };

                CategoriePersonneBll.AddCategoriePersonne(CategoriePersonne);
                SharedBll.Db.SaveChanges();

                Close();
                DXMessageBox.Show("L'ajout a été fait avec succès", "info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BackgroundColor_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (BackgroundColorStackPanel.Visibility == Visibility.Collapsed)
                {
                    BackgroundColorStackPanel.Visibility = Visibility.Visible;
                    Save.IsEnabled = false;
                    Designation.IsEnabled = false;
                    BackgroundCancelBtn.Visibility = Visibility.Visible;
                    BackgroundColorBtn.Content = "";
                    return;
                }

                BackgroundColorStackPanel.Visibility = Visibility.Collapsed;
                BackgroundColor.Background = BackgroundColorChooser.Color.ToSolidColorBrush();
                BackgroundColorBtn.Content = "";
                Save.IsEnabled = true;
                Designation.IsEnabled = true;
                BackgroundCancelBtn.Visibility = Visibility.Collapsed;
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BackgroundCancel_OnClick(object sender, RoutedEventArgs e)
        {
            BackgroundColorStackPanel.Visibility = Visibility.Collapsed;
            BackgroundColorBtn.Content = "";
            Save.IsEnabled = true;
            Designation.IsEnabled = true;
            BackgroundCancelBtn.Visibility = Visibility.Collapsed;
        }
    }
}
