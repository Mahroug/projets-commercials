﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using DevExpress.Data;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Grid;
using ProGestion.Data.Bll;
using ProGestion.Data.Layers.Entities;

namespace ProGestion.Shell.Views.Effet.Reception
{
    /// <summary>
    /// Logique d'interaction pour AddRetourFournisseurWindow.xaml
    /// </summary>
    public partial class AddRetourFournisseurWindow 
    {
        private readonly ObservableCollection<DetailsEffet> _detailEffet = new ObservableCollection<DetailsEffet>();
        private Data.Layers.Entities.Effet EffetGlobale = new Data.Layers.Entities.Effet();
        private Data.Layers.Entities.Effet EffetGlobaleTracker = new Data.Layers.Entities.Effet();
        private readonly ObservableCollection<DetailsEffet> _detailEffetFirste = new ObservableCollection<DetailsEffet>();
        private Data.Layers.Entities.Effet _Effet;
 
        public AddRetourFournisseurWindow()
        {
            InitializeComponent();
        }
        public AddRetourFournisseurWindow(Data.Layers.Entities.Effet effet)
        {
            InitializeComponent();
            ManagTableViewSecond.BestFitColumns();
            ManagGridControlSecond.TotalSummary.Add(SummaryItemType.Count, "Id");
            EffetGlobaleTracker = effet;

            try
            {

                this.EffetGlobale = new Data.Layers.Entities.Effet() {
                    MontantTTC = effet.MontantTTC,
                    DateCreation = effet.DateCreation,
                    Code = effet.Code,
                    DerniereModification = effet.DerniereModification,
                    DernierUtilisateur = effet.DernierUtilisateur,
                    EtatEffet = effet.EtatEffet,
                    Parent = effet,
                    Personne = effet.Personne,
                    TypeEffet = effet.TypeEffet,
                    Utilisateur = effet.Utilisateur,
                    Reste = effet.MontantTTC,
                };

                foreach (var e in effet.DetailsEffets)
                {
                    _detailEffetFirste.Add(new DetailsEffet()
                    {
                        Code = e.Code,
                        Lot = e.Lot,
                        PrixUnitairePaye = e.PrixUnitairePaye,
                        Quantite = 0,
                        Effet = EffetGlobale
                    });
                }

                this.EffetGlobale.DetailsEffets = _detailEffetFirste;
            }
            catch (Exception e)
            {
                DXMessageBox.Show(e.Message, "Attension", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
        }
        private void AddRetourButtonClick(object sender, RoutedEventArgs e)
        {
          
            if (!EffetGlobale.DetailsEffets.ToList().Any())
            {
                DXMessageBox.Show("Il y'a aucun produit", "Attention", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            try
            {
                _Effet = new Data.Layers.Entities.Effet
                {
                    Personne = EffetGlobale.Personne,
                    DetailsEffets = EffetGlobale.DetailsEffets.ToList(),
                    Utilisateur = SharedBll.CurrentUser,
                    DateCreation = DateTime.Now,
                    Parent = EffetGlobaleTracker,
                    TypeEffet = TypeEffetBll.GetBRF(),
                    MontantTTC = EffetGlobale.DetailsEffets.Sum(o => o.TotalPrix),
                    Reste = 0 // add window for credit
                };
                foreach (var detailsEffet in _Effet.DetailsEffets)detailsEffet.Effet = null;
                
              
                EffetBll.AddEffet(_Effet);
                LotBll.RemoveStockFournisseurLot(EffetGlobale.DetailsEffets.ToList());

                SharedBll.Db.SaveChanges();
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "erreur", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            DXMessageBox.Show("le Bon de Retour a été ajouté avec succès", "info", MessageBoxButton.OK,
                MessageBoxImage.Information);
            Close();

        }
        private void ManagTableView_OnCellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            VerifyQuantiteEffet(EffetGlobale.DetailsEffets.ToList());
            Init();
        }
        private void VerifyQuantiteEffet(List<DetailsEffet> detailsEffet)
        {
            try
            {
                foreach (var detaileffet in detailsEffet)
                {
                    var detaiQ = EffetGlobaleTracker.DetailsEffets.FirstOrDefault(d => d.Lot.Id == detaileffet.Lot.Id);
                    if (detaileffet.Quantite > detaiQ?.Quantite)
                    {
                        DXMessageBox.Show("Quantite hors le Bon de Reception", "Attention", MessageBoxButton.OK, MessageBoxImage.Warning);
                        if (EffetGlobale.DetailsEffets != null)
                            EffetGlobale.DetailsEffets.FirstOrDefault(d => d.Lot?.Id == detaiQ.Lot?.Id).Quantite =
                                detaiQ.Quantite;
                        return;
                    }
                }
            }
            catch (Exception e)
            {
                DXMessageBox.Show(e.Message, "Attension", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
        }
        private void Init()
        {
            ManagGridControlSecond.ItemsSource = null;
            ManagGridControlSecond.ItemsSource = EffetGlobale.DetailsEffets.ToList();
            TotalOrderWebLbl.Content = EffetGlobale.DetailsEffets.ToList().Sum(o => o.TotalPrix) + " DA";
        }

        private void AddRetourFournisseurWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            _detailEffet.Clear();
            ManagGridControlSecond.ItemsSource = EffetGlobale.DetailsEffets.ToList();
            TotalOrderWebLbl.Content = EffetGlobale.DetailsEffets.ToList().Sum(o => o.TotalPrix) + " DA";
        }
    }
}
