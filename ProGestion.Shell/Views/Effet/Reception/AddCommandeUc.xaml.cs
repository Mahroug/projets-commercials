﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DevExpress.Data;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Grid;
using ProGestion.Data.Bll;
using ProGestion.Data.Layers.Entities;
using ProGestion.Shell.Views.Fournisseur;

namespace ProGestion.Shell.Views.Effet.Reception
{
    /// <summary>
    /// Logique d'interaction pour AddCommandeUc.xaml
    /// </summary>
    public partial class AddCommandeUc : UserControl
    {
        private readonly ObservableCollection<DetailsEffet> _detailEffet = new ObservableCollection<DetailsEffet>();
        private Data.Layers.Entities.Effet _Effet;
        private ObservableCollection<Data.Layers.Entities.Lot> _lots = new ObservableCollection<Data.Layers.Entities.Lot>();
        public AddCommandeUc()
        {
            InitializeComponent();
            ManagTableViewFirst.BestFitColumns();
            ManagGridControlFirst.TotalSummary.Add(SummaryItemType.Count, "Id");
            ManagTableViewSecond.BestFitColumns();
            ManagGridControlSecond.TotalSummary.Add(SummaryItemType.Count, "Id");
        }

        private void AddReceptionButtonClick(object sender, RoutedEventArgs e)
        {
            var fournisseur = CboxFournisseur.SelectedItem as Data.Layers.Entities.Personne;

            if (!_detailEffet.Any())
            {
                DXMessageBox.Show("Ajouter des produits", "Attention", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            try
            {
                _Effet = new Data.Layers.Entities.Effet
                {
                    Personne = fournisseur,
                    DetailsEffets = _detailEffet.ToList(),
                    Utilisateur = SharedBll.CurrentUser,
                    DateCreation = DateTime.Now,
                    TypeEffet = TypeEffetBll.GetBC(),
                    MontantTTC = _detailEffet.Sum(o => o.TotalPrix),
                    Reste = 0 // add window for credit
                };
                EffetBll.AddEffet(_Effet);
               // just commande we musn't update stock LotBll.AddStockLot(_detailEffet.ToList());

                SharedBll.Db.SaveChanges();
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "erreur", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            DXMessageBox.Show("le Bon de Commande a été ajouté avec succès", "info", MessageBoxButton.OK,
                MessageBoxImage.Information);
            AddReception_OnLoaded(null, null);

        }

        private void Btn_addFournisseur(object sender, RoutedEventArgs e)
        {
            try
            {
                AddFournisseurWindow addFournisseurWindow = new AddFournisseurWindow();
                addFournisseurWindow.ShowDialog();
                // Refresh_();
                var f = SharedBll.Db.Personnes.Local.Where(c => c.TypePersonne.Id == 2);
                if (f.Any())
                {
                    CboxFournisseur.ItemsSource = f.ToArray();
                    CboxFournisseur.SelectedItem = f.ToArray().LastOrDefault();
                }
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Attension", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }


        }

        private void DeleteItem_Click(object sender, RoutedEventArgs e)
        {
            var detailsEffet = ManagGridControlSecond.SelectedItem as DetailsEffet;
            if (detailsEffet == null)return;
            
                _detailEffet.Remove(detailsEffet);
                TotalOrderWebLbl.Content = _detailEffet.Sum(o => o.TotalPrix) + " DA";
            
        }

        private void ManagTableView_OnCellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            try
            {
                if (e.Column.Name == "QntColumnSecond")
                {
                    _detailEffet[e.RowHandle].Quantite = Convert.ToInt32(e.Value.ToString());
                }
                if (e.Column.Name == "PrixColumnSecond")
                {
                    _detailEffet[e.RowHandle].PrixUnitairePaye = Convert.ToInt32(e.Value.ToString());
                }
                ManagGridControlSecond.ItemsSource = null;
                ManagGridControlSecond.ItemsSource = _detailEffet;

                TotalOrderWebLbl.Content = _detailEffet.Sum(o => o.TotalPrix) + " DA";

                if (e != null)
                {
                    var index = e.RowHandle;
                    var modifeditem = new DetailsEffet();
                    var newitem = new DetailsEffet();
                    modifeditem = _detailEffet[index];

                    if (modifeditem != null)
                    {
                        switch (e.Column.Name)
                        {
                            case "QntColumn":
                                if (modifeditem.Lot != null)
                                {
                                    if (e.Value != null)
                                        newitem = new DetailsEffet
                                        {
                                            Lot = modifeditem.Lot,
                                            PrixUnitairePaye = modifeditem.PrixUnitairePaye,
                                            Quantite = Convert.ToInt32(e.Value.ToString()),
                                            Effet = modifeditem.Effet,
                                        };
                                }
                                break;
                        }
                    }
                    TotalOrderWebLbl.Content = _detailEffet.Sum(o => o.TotalPrix) + " DA";
                    ManagGridControlSecond.ItemsSource = null;
                    ManagGridControlSecond.ItemsSource = _detailEffet;

                }
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Attension", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }

        }

        private void AddProductClick(object sender, RoutedEventArgs e)
        {
            if (!(ManagGridControlFirst.SelectedItem is Data.Layers.Entities.Lot lot))

            {
                DXMessageBox.Show("Selectionnez un produit", "Attention", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            try
            {
                var isfounded = false;
                foreach (var item in _detailEffet)
                {
                    if (item.Lot.Id == lot.Id)
                    {
                        //item.PrixUnitairePaye = (double)PrixUnitaireBox.Value;
                        item.Quantite += 1;//(int)QuantityBox.Value;
                        isfounded = true;
                        break;
                    }
                }
                if (!isfounded)
                {
                    _detailEffet.Add(new DetailsEffet
                    {
                        Lot = lot,
                        PrixUnitairePaye = lot.PrixAchat,//(double)PrixUnitaireBox.Value,
                        Quantite = 1//(int)QuantityBox.Value
                    });
                }
                TotalOrderWebLbl.Content = _detailEffet.Sum(o => o.TotalPrix) + " DA";
                Init();
                SearchBox.Text = "";

            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Attension", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
        }

        private void Init()
        {
            ManagGridControlSecond.ItemsSource = null;
            ManagGridControlSecond.ItemsSource = _detailEffet;
        }

        private void AddReception_OnLoaded(object sender, RoutedEventArgs e)
        {
            try
            {
                _lots = null;
                _lots = new ObservableCollection<Data.Layers.Entities.Lot>(SharedBll.Db.Lots.Local);
                ManagGridControlFirst.ItemsSource = null;
                CboxFournisseur.ItemsSource = null;
                ManagGridControlFirst.ItemsSource = _lots;
                _detailEffet.Clear();

                var f = SharedBll.Db.Personnes.Local.Where(c => c.TypePersonne.Id == 2);
                if (f.Any())
                {
                    CboxFournisseur.ItemsSource = f.ToArray();

                }
                ManagGridControlSecond.ItemsSource = _detailEffet;
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Attension", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
        }

        private void SearchBox_OnKeyUp(object sender, KeyEventArgs e)
        {
            Search(SearchBox.Text.Trim() != String.Empty);
        }
        private void Search(bool keyUpEmptyText)
        {
            try
            {
                ManagGridControlFirst.ItemsSource = null;

                if (SearchBox.Text.Trim() == string.Empty && keyUpEmptyText)
                {
                    ManagGridControlFirst.ItemsSource = _lots;
                    return;
                }

                ManagGridControlFirst.ItemsSource = null;
                ManagGridControlFirst.ItemsSource = _lots.Where(c =>
                    c.Produit?.Designation != null && c.Produit.Designation.ToLower().Contains(SearchBox.Text.ToLower()) ||
                    c.CodeBar != null && c.CodeBar.ToLower().Contains(SearchBox.Text.ToLower())
                );
            }
            catch (Exception e)
            {
                DXMessageBox.Show(e.Message, "Attention", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
        }

        private void GenerateButtonClick(object sender, RoutedEventArgs e)
        {
            try
            {
                ManagGridControlSecond.ItemsSource = null;
                var ls = SharedBll.Db.Lots.ToList();
                if (!ls.Any())return;
                
                foreach (var l in ls)
                {
                    if(l?.Produit?.Quantite <= l.Produit?.stockMinimum)
                    {
                        _detailEffet.Add(new DetailsEffet
                        {
                            Lot = l,
                            PrixUnitairePaye = l.PrixAchat,//(double)PrixUnitaireBox.Value,
                            Quantite = l.Produit.stockMinimum - l.Produit.Quantite + 1//(int)QuantityBox.Value
                        });
                    }
                }
                ManagGridControlSecond.ItemsSource = null;
                ManagGridControlSecond.ItemsSource = _detailEffet;
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Attention", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
        }

    }
}