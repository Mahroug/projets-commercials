﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.Xpf.Core;
using ProGestion.Data.Bll;
using ProGestion.Data.Layers.Entities;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using UserControl = System.Windows.Controls.UserControl;

namespace ProGestion.Shell.Views.Effet.Reception
{
    /// <summary>
    /// Logique d'interaction pour CommandeUc.xaml
    /// </summary>
    public partial class CommandeUc : UserControl
    {
        private ObservableCollection<Data.Layers.Entities.Effet> _manageSource =
            new ObservableCollection<Data.Layers.Entities.Effet>();
        public CommandeUc()
        {
            InitializeComponent();
            Refresh();
            ManagTableView.BestFitColumns();
            ManagGridControl.TotalSummary.Add(SummaryItemType.Count, "Id");
        }

        private void Refresh()
        {
            _manageSource = null;
            _manageSource = new ObservableCollection<Data.Layers.Entities.Effet>(SharedBll.Db.Effets.Local.Where(e => e.TypeEffet?.Abreviation == "BC"));

            if (!_manageSource.Any()) return;


            try
            {

                ManagGridControl.ItemsSource = null;
                ManagTableView.RowMinHeight = SharedBll.UcFontSize * 2.85;
                Search(true);

            }
            catch (Exception e)
            {
                DXMessageBox.Show(e.Message, "error", MessageBoxButton.OK, MessageBoxImage.Error);

            }
        }

        private void AddReceptionClick(object sender, RoutedEventArgs e)
        {
            var parentWindow = Window.GetWindow(this) as MainWindow;
            parentWindow?.MainNavigationFrame.Navigate(new AddCommandeUc());

            Refresh();
        }

        private void SearchBox_OnKeyUp(object sender, KeyEventArgs e)
        {
            Search(SearchBox.Text.Trim() != String.Empty);
        }

        private void Search(bool keyUpEmptyText)
        {
            try
            {
                ManagGridControl.ItemsSource = null;

                if (SearchBox.Text.Trim() == String.Empty && keyUpEmptyText)
                {
                    ManagGridControl.ItemsSource =
                        new ObservableCollection<Data.Layers.Entities.Effet>(SharedBll.Db.Effets.Local.Where(e => e.TypeEffet?.Abreviation == "BC"));
                    return;
                }

                ManagGridControl.ItemsSource = null;
                ManagGridControl.ItemsSource = _manageSource.Where(c =>
                    (c.Personne?.FullName != null &&
                     c.Personne.FullName.ToLower().Contains(SearchBox.Text.ToLower())) ||
                    (c.Id != 0 && c.Id.ToString().Contains(SearchBox.Text.ToLower())) ||
                        (c.DetailsEffets.Any(l => l.Lot.CodeBar.Contains(SearchBox.Text.ToLower()))
                        ));
            }
            catch (Exception e)
            {
                DXMessageBox.Show(e.Message, "Attension", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
        }

        private void DeleteReceptionClick(object sender, RoutedEventArgs e)
        {
            if (ManagGridControl.SelectedItem == null) return;

            Data.Layers.Entities.Effet effet;
            try
            {
                effet = ManagGridControl.SelectedItem as Data.Layers.Entities.Effet;
                var result = DXMessageBox.Show("Voulez vous supprimer cette ligne?", "Info", MessageBoxButton.YesNo,
                    MessageBoxImage.Warning);
                if (result != MessageBoxResult.Yes) return;

                EffetBll.RemoveEffet(effet);
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            SharedBll.Db.SaveChanges();
            DXMessageBox.Show("suppression avec succès", "info", MessageBoxButton.OK, MessageBoxImage.Information);
            Search(true);
        }

        private void Excel_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var saveFileDialog = new SaveFileDialog
                {
                    InitialDirectory = Convert.ToString(Environment.SpecialFolder.Desktop),
                    Filter = @"(*.XLSX)|*.xlsx|All Files (*.*)|*.*",
                    FilterIndex = 1,
                    FileName = "List"
                };

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                    ManagTableView.ExportToXlsx(saveFileDialog.FileName);
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void DeleteDetailsClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var result = DXMessageBox.Show("Êtes-vous sûr de vouloir supprimer cette ligne ?", "Info",
                    MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (result != MessageBoxResult.Yes) return;

                if (ChildGridControl.SelectedItem is DetailsEffet item)
                {
                    DetailsEffetBll.RemoveDetails(item);
                }
                SharedBll.Db.SaveChanges();
                ManagGridControl.RefreshData();
                DXMessageBox.Show("Suppression avec succés ", "Info",
                    MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Attension", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
        }

    }
}