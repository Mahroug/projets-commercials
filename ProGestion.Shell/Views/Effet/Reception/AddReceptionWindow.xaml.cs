﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Editors;
using DevExpress.Xpf.Grid;
using ProGestion.Data.Bll;
using ProGestion.Data.Layers.Entities;
using ProGestion.Shell.Views.Client;

namespace ProGestion.Shell.Views.Effet.Reception
{
    /// <summary>
    /// Logique d'interaction pour AddReceptionWindow.xaml
    /// </summary>
    public partial class AddReceptionWindow
    {
        private readonly ObservableCollection<DetailsEffet> _detailEffet = new ObservableCollection<DetailsEffet>();
        private Data.Layers.Entities.Effet _Effet;
        public AddReceptionWindow()
        {
            InitializeComponent();
        }
        private void AddProductClick(object sender, RoutedEventArgs e)
        {
            var lot = CboxProduit.SelectedItem as Data.Layers.Entities.Lot;
            if (lot == null)
            {
                DXMessageBox.Show("Selectionnez un produit", "Attention", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }


            var detaileffet = new DetailsEffet
            {
                Lot = lot,
                PrixUnitairePaye = (double)PrixUnitaireBox.Value,
                Quantite = (int)QuantityBox.Value,
            };
            var isfounded = false;
            DetailsEffet detailEffetFounded = new DetailsEffet();
            int index = 0;
            foreach (var item in _detailEffet)
            {
                if (item.Lot.Id == detaileffet.Lot.Id)
                {
                    isfounded = true;
                    detailEffetFounded = new DetailsEffet
                    {
                        Id = item.Id,
                        Lot = item.Lot,
                        PrixUnitairePaye = item.PrixUnitairePaye,
                        Quantite = item.Quantite,
                        Effet = item.Effet
                    };

                    break;
                }
                index++;
            }
            if (isfounded)
            {
                _detailEffet[index] = new DetailsEffet
                {
                    Id = detailEffetFounded.Id,
                    Lot = detailEffetFounded.Lot,
                    PrixUnitairePaye = detailEffetFounded.PrixUnitairePaye,
                    Quantite = detaileffet.Quantite + detailEffetFounded.Quantite,
                    Effet = detailEffetFounded.Effet
                };
            }
            else
            {
                _detailEffet.Add(detaileffet);
            }


            TotalOrderWebLbl.Content = _detailEffet.Sum(o => o.TotalPrix) + " DA";
            Init();
        }

        private void Init()
        {
            PrixUnitaireBox.Value = 0;
            QuantityBox.Value = 1;
            CboxClient.SelectedItem = null;
            CboxCategory.SelectedItem = null;
            CboxBrand.SelectedItem = null;
            CboxProduit.ItemsSource = SharedBll.Db.Lots.Local.ToArray();
            CboxProduit.SelectedItem = null;
            TotalLbl.Content = "0";
        }

        private void AddReception_OnLoaded(object sender, RoutedEventArgs e)
        {
            CboxClient.ItemsSource = SharedBll.Db.Personnes.Local.ToArray().Where(c => c.TypePersonne.Id == 2);
            CboxCategory.ItemsSource = SharedBll.Db.CategorieProduits.Local.ToArray();
            CboxBrand.ItemsSource = SharedBll.Db.Marques.Local.ToArray();
            CboxProduit.ItemsSource = SharedBll.Db.Lots.Local.ToArray();
            ManagGridControl.ItemsSource = _detailEffet;
        }
        // params price isEnable = [ params value ]
        private void CboxProduit_OnSelected(object sender, RoutedEventArgs routedEventArgs)
        {
            var lot = CboxProduit.SelectedItem as Data.Layers.Entities.Lot;
            if (lot == null) return;
            PrixUnitaireBox.Value = (decimal)lot.PrixAchat;
            TotalLbl.Content = (int)QuantityBox.Value * (decimal)lot.PrixAchat + " DA";

        }

        private void AddReceptionButtonClick(object sender, RoutedEventArgs e)
        {
            var client = CboxClient.SelectedItem as Data.Layers.Entities.Personne;
            if (client == null)
            {
                DXMessageBox.Show("Selectionnez un client", "Attention", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            if (!_detailEffet.Any())
            {
                DXMessageBox.Show("Ajouter des produits", "Attention", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            //CancelBtn.IsEnabled = false;
            //LabelMsg.Visibility = Visibility.Visible;
            try
            {
                _Effet = new Data.Layers.Entities.Effet
                {
                    Personne = client,
                    DetailsEffets = _detailEffet,
                    Utilisateur = SharedBll.CurrentUser,
                    DateCreation = DateTime.Now,
                    TypeEffet = new TypeEffet()
                    {
                        Designation = "BonReception",
                        Abreviation = "BR",
                    },
                    MontantTTC = _detailEffet.Sum(o => o.TotalPrix),
                    Versement = 0 // add window for credit
                };
                EffetBll.AddEffet(_Effet);


                SharedBll.Db.SaveChanges();
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "erreur", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
           
            DXMessageBox.Show("le Bon de Reception a été ajouté avec succès", "info", MessageBoxButton.OK,
                MessageBoxImage.Information);
            
            Close();
        }

        private void Btn_addClient(object sender, RoutedEventArgs e)
        {
            AddClientWindow addClientWindow = new AddClientWindow();
            addClientWindow.ShowDialog();
            // Refresh_();
            if (SharedBll.Db.Personnes.Where(c=>c.TypePersonne.Id == 2) != null)
            {
                CboxClient.ItemsSource = SharedBll.Db.Personnes.Local.ToArray().Where(c => c.TypePersonne?.Id == 2);
            }
            CboxClient.SelectedItem = SharedBll.Db.Personnes.Local.ToArray().LastOrDefault(c => c.TypePersonne?.Id == 2);

        }

        private void DeleteItem_Click(object sender, RoutedEventArgs e)
        {
            var detailsEffet = ManagGridControl.SelectedItem as DetailsEffet;
            if (detailsEffet != null)
            {
                _detailEffet.Remove(detailsEffet);
                TotalOrderWebLbl.Content = _detailEffet.Sum(o => o.TotalPrix) + " DA";
            }
        }

        private void QuantityBox_OnEditValueChanged(object sender, EditValueChangedEventArgs e)
        {
            var lot = CboxProduit.SelectedItem as Data.Layers.Entities.Lot;
            if (lot == null) return;
            TotalLbl.Content = (int)QuantityBox.Value * (decimal)lot.PrixAchat + " DA";
        }

        private void ManagTableView_OnCellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            if (e != null)
            {
                var index = e.RowHandle;
                var modifeditem = new DetailsEffet();
                var newitem = new DetailsEffet();
                modifeditem = _detailEffet[index];

                if (modifeditem != null)
                {
                    newitem = new DetailsEffet
                    {
                        Id = modifeditem.Id,
                        Lot = modifeditem.Lot,
                        PrixUnitairePaye = modifeditem.PrixUnitairePaye,
                        Quantite = modifeditem.Quantite,
                        Effet = modifeditem.Effet,
                    };

                    switch (e.Column.Name)
                    {
                        case "QntColumn":
                            if (modifeditem.Effet != null && modifeditem.Lot != null)
                            {
                                if (e.Value != null)
                                    newitem = new DetailsEffet
                                    {
                                        Id = modifeditem.Id,
                                        Lot = modifeditem.Lot,
                                        PrixUnitairePaye = modifeditem.PrixUnitairePaye,
                                        Quantite = Convert.ToInt32(e.Value.ToString()),
                                        Effet = modifeditem.Effet,
                                    };
                            }
                            break;
                    }
                }
            }
            TotalOrderWebLbl.Content = _detailEffet.Sum(o => o.TotalPrix) + " DA";
        }

        private void CboxBrand_OnSelected(object sender, RoutedEventArgs routedEventArgs)
        {
            if (CboxBrand.SelectedItem != null && CboxCategory.SelectedItem == null)
                CboxProduit.ItemsSource = SharedBll.Db.Lots.Local.ToArray().Where(c => c.Produit.Marque == CboxBrand.SelectedItem);

            else if (CboxCategory.SelectedItem != null || CboxBrand.SelectedItem != null)
                CboxProduit.ItemsSource = SharedBll.Db.Lots.Local.ToArray().Where(c => c.Produit.Marque == CboxBrand.SelectedItem && c.Produit.CategorieProduit == CboxCategory.SelectedItem);

        }
        private void CboxCategory_OnSelected(object sender, RoutedEventArgs routedEventArgs)
        {
            if (CboxCategory.SelectedItem != null && CboxBrand.SelectedItem == null)
                CboxProduit.ItemsSource = SharedBll.Db.Lots.Local.ToArray().Where(c => c.Produit.CategorieProduit == CboxCategory.SelectedItem);

            else if (CboxCategory.SelectedItem != null || CboxBrand.SelectedItem != null)
                CboxProduit.ItemsSource = SharedBll.Db.Lots.Local.ToArray().Where(c => c.Produit.CategorieProduit == CboxCategory.SelectedItem && c.Produit.Marque == CboxBrand.SelectedItem);

        }

        private void QuantityBox_OnKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                AddProductClick(sender, e);
        }

        private void ButtonRefresh_OnClick(object sender, RoutedEventArgs e)
        {
            Init();
        }
    }
}
