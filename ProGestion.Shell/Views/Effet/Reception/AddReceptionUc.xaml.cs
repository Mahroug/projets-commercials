﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DevExpress.Data;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Grid;
using ProGestion.Data.Bll;
using ProGestion.Data.Layers.Entities;
using ProGestion.Shell.Views.Fournisseur;

namespace ProGestion.Shell.Views.Effet.Reception
{
    public partial class AddReceptionUc : UserControl
    {
        private int workflow = 0;
        private bool NotWork = false;
        private readonly ObservableCollection<DetailsEffet> _detailEffet = new ObservableCollection<DetailsEffet>();
        private Data.Layers.Entities.Effet _Effet;
        private ObservableCollection<Data.Layers.Entities.Lot> _lots = new ObservableCollection<Data.Layers.Entities.Lot>();
        public AddReceptionUc()
        {
            InitializeComponent();
            ManagTableViewFirst.BestFitColumns();
            ManagGridControlFirst.TotalSummary.Add(SummaryItemType.Count, "Id");
            ManagTableViewSecond.BestFitColumns();
            ManagGridControlSecond.TotalSummary.Add(SummaryItemType.Count, "Id");
        }

        private void AddReceptionButtonClick(object sender, RoutedEventArgs e)
        {
            var fournisseur = CboxFournisseur.SelectedItem as Data.Layers.Entities.Personne;

            if (!_detailEffet.Any())
            {
                NotWork = true;
                DXMessageBox.Show("Ajouter des produits", "Attention", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            try
            {
                _Effet = new Data.Layers.Entities.Effet
                {
                    Personne = fournisseur,
                    DetailsEffets = _detailEffet.ToList(),
                    Utilisateur = SharedBll.CurrentUser,
                    DateCreation = DateTime.Now,
                    TypeEffet = TypeEffetBll.GetBR(),
                    MontantTTC = _detailEffet.Sum(o => o.TotalPrix),
                    Reste = 0 // add window for credit
                };
                EffetBll.AddEffet(_Effet);
                LotBll.AddStockLotFournisseur(_detailEffet.ToList());

                SharedBll.Db.SaveChanges();
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "erreur", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            NotWork = true;
            DXMessageBox.Show("le Bon de Reception a été ajouté avec succès", "info", MessageBoxButton.OK,
                MessageBoxImage.Information);
            AddReception_OnLoaded(null, null);

        }

        private void Btn_addFournisseur(object sender, RoutedEventArgs e)
        {
            try
            {
                AddFournisseurWindow addFournisseurWindow = new AddFournisseurWindow();
                addFournisseurWindow.ShowDialog();
            
                var f = SharedBll.Db.Personnes.Local.Where(c => c.TypePersonne.Id == 2).ToList();
                if (!f.Any()) return;
                CboxFournisseur.ItemsSource = f.ToArray();
                CboxFournisseur.SelectedItem = f.ToArray().LastOrDefault();
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Attension", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
        }

        private void DeleteItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var detailsEffet = ManagGridControlSecond.SelectedItem as DetailsEffet;
                if (detailsEffet != null)
                {
                    _detailEffet.Remove(detailsEffet);
                    TotalOrderWebLbl.Content = _detailEffet.Sum(o => o.TotalPrix) + " DA";
                }
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Attension", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
        }

        private void ManagTableView_OnCellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            ManagGridControlSecond.ItemsSource = null;
            ManagGridControlSecond.ItemsSource = _detailEffet;

            TotalOrderWebLbl.Content = _detailEffet.Sum(o => o.TotalPrix) + " DA";
            
        }

        private void AddProductClick(object sender, RoutedEventArgs e)
        {
            if (!(ManagGridControlFirst.SelectedItem is Data.Layers.Entities.Lot lot))

            {
                NotWork = true;
                DXMessageBox.Show("Selectionnez un produit", "Attention", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }


            try
            {
                var isfounded = false;
                foreach (var item in _detailEffet)
                {
                    if (item?.Lot?.Id == lot.Id)
                    {
                        //item.PrixUnitairePaye = (double)PrixUnitaireBox.Value;
                        item.Quantite += 1;//(int)QuantityBox.Value;
                        isfounded = true;
                        break;
                    }
                }
                if (!isfounded)
                {
                    _detailEffet.Add(new DetailsEffet
                    {
                        Lot = lot,
                        PrixUnitairePaye = lot.PrixAchat,//(double)PrixUnitaireBox.Value,
                        Quantite = 1//(int)QuantityBox.Value
                    });
                }
                TotalOrderWebLbl.Content = _detailEffet.Sum(o => o.TotalPrix) + " DA";
                Init();
                SearchBox.Text = "";
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Attension", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
        }
        private void AddProductKeyClick()
        {
            try
            {
                var lot = _lots.Where(c =>
                    (c.Produit?.Designation != null &&
                     c.Produit.Designation.ToLower().Contains(SearchBox.Text.ToLower())) ||
                    (c.CodeBar != null && c.CodeBar.ToLower().Contains(SearchBox.Text.ToLower()))).FirstOrDefault();

                if (lot == null) return;


                var isfounded = false;
                foreach (var item in _detailEffet)
                {
                    if (item.Lot.Id == lot.Id)
                    {
                        item.Quantite += 1;
                        isfounded = true;
                        break;
                    }
                }
                if (!isfounded)
                {
                    _detailEffet.Add(new DetailsEffet
                    {
                        Lot = lot,
                        PrixUnitairePaye = lot.PrixAchat,
                        Quantite = 1
                    });
                }
                TotalOrderWebLbl.Content = _detailEffet.Sum(o => o.TotalPrix) + " DA";
                Init();
                SearchBox.Text = "";
            }
            catch (Exception e)
            {
                DXMessageBox.Show(e.Message, "Attension", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
        }

        private void Init()
        {
            ManagGridControlSecond.ItemsSource = null;
            ManagGridControlSecond.ItemsSource = _detailEffet;
        }

        private void AddReception_OnLoaded(object sender, RoutedEventArgs e)
        {
            try
            {
                _lots = null;
                _lots = new ObservableCollection<Data.Layers.Entities.Lot>(SharedBll.Db.Lots.Local);
                ManagGridControlFirst.ItemsSource = null;
                CboxFournisseur.ItemsSource = null;
                ManagGridControlFirst.ItemsSource = _lots;
                _detailEffet.Clear();

                var f = SharedBll.Db.Personnes.Where(c => c.TypePersonne.Id == 2);
                if (f.Any())
                {
                    CboxFournisseur.ItemsSource = f.ToArray();
                }
                ManagGridControlSecond.ItemsSource = _detailEffet;
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Attension", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
        }

        private void SearchBox_OnKeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (workflow == 1)
                {
                    ManagTableViewSecond.ClearSelection();
                    ManagTableViewSecond.FocusedRowHandle = (_detailEffet.ToList().Count - 1);
                    ManagTableViewSecond.SelectRow(1);
                    ManagTableViewSecond.FocusedColumn = ManagTableViewSecond.VisibleColumns[1];
                    ManagTableViewSecond.FocusedRowHandle = (_detailEffet.ToList().Count - 1);
                    ManagTableViewSecond.ShowEditor();
                    workflow = 0;
                }
                else
                {
                    Search(SearchBox.Text.Trim() != String.Empty);
                    if (e.Key == Key.Enter)
                    {
                        if (NotWork)
                        {
                            NotWork = false;
                            return;
                        }
                        AddProductKeyClick();
                        workflow = 1;
                    }

                }
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Attension", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
            
        }
        private void Search(bool keyUpEmptyText)
        {
            try
            {
                ManagGridControlFirst.ItemsSource = null;

                if (SearchBox.Text.Trim() == String.Empty && keyUpEmptyText)
                {
                    ManagGridControlFirst.ItemsSource = _lots;
                    return;
                }

                ManagGridControlFirst.ItemsSource = null;
                ManagGridControlFirst.ItemsSource = _lots.Where(c => (c.Produit?.Designation != null && c.Produit.Designation.ToLower().Contains(SearchBox.Text.ToLower())) ||
                                                                     (c.CodeBar != null && c.CodeBar.ToLower().Contains(SearchBox.Text.ToLower()))
                );
            }
            catch (Exception e)
            {
                DXMessageBox.Show(e.Message, "Attension", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
        }

        private void AddReceptionUc_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == (Key.F1)) SearchBox.Focus();
            if (e.Key == (Key.F2)) AddReceptionButtonClick(null, null);
        }
    }
}
