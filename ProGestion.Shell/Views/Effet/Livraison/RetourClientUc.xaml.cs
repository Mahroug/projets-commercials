﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.Xpf.Core;
using ProGestion.Data.Bll;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using UserControl = System.Windows.Controls.UserControl;

namespace ProGestion.Shell.Views.Effet.Livraison
{
    /// <summary>
    /// Logique d'interaction pour RetourClientUc.xaml
    /// </summary>
    public partial class RetourClientUc : UserControl
    {
        private ObservableCollection<Data.Layers.Entities.Effet> _manageSource =
             new ObservableCollection<Data.Layers.Entities.Effet>();
        public RetourClientUc()
        {
            InitializeComponent();
            Refresh();
            ManagTableView.BestFitColumns();
            ManagGridControl.TotalSummary.Add(SummaryItemType.Count, "Id");
        }
        private void Refresh()
        {
            _manageSource = null;
            _manageSource = new ObservableCollection<Data.Layers.Entities.Effet>(SharedBll.Db.Effets.Local.Where(e => e.TypeEffet?.Abreviation == "BRC"));

            if (!_manageSource.Any()) return;
            try
            {

                ManagGridControl.ItemsSource = null;
                ManagTableView.RowMinHeight = SharedBll.UcFontSize * 2.85;
                Search(true);

            }
            catch (Exception e)
            {
                DXMessageBox.Show(e.Message, "error", MessageBoxButton.OK, MessageBoxImage.Error);

            }
        }
        private void SearchBox_OnKeyUp(object sender, KeyEventArgs e)
        {
            Search(SearchBox.Text.Trim() != String.Empty);
        }

        private void Search(bool keyUpEmptyText)
        {
            ManagGridControl.ItemsSource = null;

            try
            {
                if (SearchBox.Text.Trim() == String.Empty && keyUpEmptyText)
                {
                    ManagGridControl.ItemsSource =
                        new ObservableCollection<Data.Layers.Entities.Effet>(SharedBll.Db.Effets.Local.Where(e => e.TypeEffet?.Abreviation == "BRC"));
                    return;
                }

                ManagGridControl.ItemsSource = null;
                ManagGridControl.ItemsSource = _manageSource.Where(c =>
                    (c.Personne?.FullName != null &&
                     c.Personne.FullName.ToLower().Contains(SearchBox.Text.ToLower())) ||
                    (c.Id != 0 && c.Id.ToString().Contains(SearchBox.Text.ToLower())) ||
                    (c.DetailsEffets.Any(l => l.Lot.CodeBar.Contains(SearchBox.Text.ToLower())))
                );
            }
            catch (Exception e)
            {
                DXMessageBox.Show(e.Message, "Attension", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
        }

        private void Excel_OnClick(object sender, RoutedEventArgs e)
        {

            try
            {
                var saveFileDialog = new SaveFileDialog
                {
                    InitialDirectory = Convert.ToString(Environment.SpecialFolder.Desktop),
                    Filter = @"(*.XLSX)|*.xlsx|All Files (*.*)|*.*",
                    FilterIndex = 1,
                    FileName = "List"
                };

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                    ManagTableView.ExportToXlsx(saveFileDialog.FileName);
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);

            }

        }
        
    }
}
