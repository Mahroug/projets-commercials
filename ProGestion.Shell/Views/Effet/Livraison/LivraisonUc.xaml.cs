﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.Xpf.Core;
using ProGestion.Data.Bll;
using ProGestion.Data.Layers.Entities;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using UserControl = System.Windows.Controls.UserControl;

namespace ProGestion.Shell.Views.Effet.Livraison
{
    /// <summary>
    /// Logique d'interaction pour LivraisonUc.xaml
    /// </summary>
    public partial class LivraisonUc : UserControl
    {
        private ObservableCollection<Data.Layers.Entities.Effet> _manageSource =
            new ObservableCollection<Data.Layers.Entities.Effet>();
        public LivraisonUc()
        {
            InitializeComponent(); Refresh();
            ManagTableView.BestFitColumns();
            ManagGridControl.TotalSummary.Add(SummaryItemType.Count, "Id");
        }

        private void Refresh()
        {
            _manageSource = null;
            _manageSource = new ObservableCollection<Data.Layers.Entities.Effet>(SharedBll.Db.Effets.Local.Where(e => e.TypeEffet?.Abreviation == "BL"));

            if (!_manageSource.Any()) return;
            try
            {

                ManagGridControl.ItemsSource = null;
                ManagTableView.RowMinHeight = SharedBll.UcFontSize * 2.85;
                Search(true);

            }
            catch (Exception e)
            {
                DXMessageBox.Show(e.Message, "error", MessageBoxButton.OK, MessageBoxImage.Error);

            }
        }

        private void AddLivraisonClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var parentWindow = Window.GetWindow(this) as MainWindow;
                parentWindow?.MainNavigationFrame.Navigate(new AddLivraisonUc());

                Refresh();
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Attension", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
        }

        private void SearchBox_OnKeyUp(object sender, KeyEventArgs e)
        {
            Search(SearchBox.Text.Trim() != String.Empty);
        }

        private void Search(bool keyUpEmptyText)
        {
            ManagGridControl.ItemsSource = null;

            if (SearchBox.Text.Trim() == String.Empty && keyUpEmptyText)
            {
                ManagGridControl.ItemsSource =
                    new ObservableCollection<Data.Layers.Entities.Effet>(SharedBll.Db.Effets.Local.Where(e => e.TypeEffet?.Abreviation == "BL"));
                return;
            }

            ManagGridControl.ItemsSource = null;
            ManagGridControl.ItemsSource = _manageSource.Where(c =>
                (c.Personne?.FullName != null &&
                 c.Personne.FullName.ToLower().Contains(SearchBox.Text.ToLower())) ||
                (c.Id != 0 && c.Id.ToString().Contains(SearchBox.Text.ToLower())) ||
                (c.DetailsEffets.Any(l => l.Lot.CodeBar.Contains(SearchBox.Text.ToLower()))
            ));
        }

        private void Excel_OnClick(object sender, RoutedEventArgs e)
        {

            try
            {
                var saveFileDialog = new SaveFileDialog
                {
                    InitialDirectory = Convert.ToString(Environment.SpecialFolder.Desktop),
                    Filter = @"(*.XLSX)|*.xlsx|All Files (*.*)|*.*",
                    FilterIndex = 1,
                    FileName = "List"
                };

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                    ManagTableView.ExportToXlsx(saveFileDialog.FileName);
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);

            }

        }

        private void MenuItemBRC_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var effet = ManagGridControl.SelectedItem as Data.Layers.Entities.Effet;
                if (effet == null) return;
                var effetparent = SharedBll.Db.Effets.Local.FirstOrDefault(ee => ee.Parent?.Id == effet.Id);
                if (effetparent != null)
                {
                    var resu = DXMessageBox.Show("Cet bon est déja retourné , veuillez vous modifié? ", "Question",
                        MessageBoxButton.YesNo,
                        MessageBoxImage.Question);
                    if (resu != MessageBoxResult.Yes) return;
                    new EditRetourClientWindow(effetparent).ShowDialog();
                    Refresh();
                    return;
                }
                new AddRetourClientWindow(effet).ShowDialog();
                Refresh();
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Attension", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
        }

        private void ReglerDetteClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!(ManagGridControl.SelectedItem is Data.Layers.Entities.Effet effet)) return;
                EffetBll.ReglerDette(effet);
                SharedBll.Db.SaveChanges();
                Refresh();
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Attension", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
        }
    }          

}