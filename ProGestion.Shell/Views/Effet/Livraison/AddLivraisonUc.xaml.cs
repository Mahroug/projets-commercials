﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;
using DevExpress.Data;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Grid;
using ProGestion.Data.Bll;
using ProGestion.Data.Layers.Entities;
using ProGestion.Shell.Views.Client;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using UserControl = System.Windows.Controls.UserControl;

namespace ProGestion.Shell.Views.Effet.Livraison
{
    public partial class AddLivraisonUc : UserControl
    {
        private int workflow = 0;
        private bool NotWork = false;
        private readonly ObservableCollection<DetailsEffet> _detailEffet = new ObservableCollection<DetailsEffet>();
        private Data.Layers.Entities.Effet _Effet;
        private ObservableCollection<Data.Layers.Entities.Lot> _lots = new ObservableCollection<Data.Layers.Entities.Lot>();
        public AddLivraisonUc()
        {
            InitializeComponent();
            ManagTableViewFirst.BestFitColumns();
            ManagGridControlFirst.TotalSummary.Add(SummaryItemType.Count, "Id");
            ManagTableViewSecond.BestFitColumns();
            ManagGridControlSecond.TotalSummary.Add(SummaryItemType.Count, "Id");

            var effet = SharedBll.Db.Effets.FirstOrDefault(e => e.TypeEffet.Abreviation == "BROL");
            if (effet != null)
            {
                _Effet = effet;
                TotalOrderWebLbl.Content = _Effet.DetailsEffets.ToList().Sum(o => o.TotalPrix) + " DA";
            }
            else
            {
                _Effet = new Data.Layers.Entities.Effet
                {
                    DetailsEffets = new ObservableCollection<DetailsEffet>(),
                    Utilisateur = SharedBll.CurrentUser,
                    DateCreation = DateTime.Now,
                    TypeEffet = TypeEffetBll.GetEffet("BROL"),
                    MontantTTC = 0,
                    PrixAchatTotal = 0,
                    Reste = 0 // add window for credit
                };
                EffetBll.AddEffet(_Effet);
                SharedBll.Db.SaveChanges();
               
            }
        }
        public AddLivraisonUc(Data.Layers.Entities.Lot lot)
        {
            InitializeComponent();
            ManagTableViewFirst.BestFitColumns();
            ManagGridControlFirst.TotalSummary.Add(SummaryItemType.Count, "Id");
            ManagTableViewSecond.BestFitColumns();
            ManagGridControlSecond.TotalSummary.Add(SummaryItemType.Count, "Id");

            var effet = SharedBll.Db.Effets.FirstOrDefault(e => e.TypeEffet.Abreviation == "BROL");
            if (effet != null)
            {
                _Effet = effet;
                TotalOrderWebLbl.Content = _Effet.DetailsEffets.ToList().Sum(o => o.TotalPrix) + " DA";
            }
            else
            {
                _Effet = new Data.Layers.Entities.Effet
                {
                    DetailsEffets = new ObservableCollection<DetailsEffet>(),
                    Utilisateur = SharedBll.CurrentUser,
                    DateCreation = DateTime.Now,
                    TypeEffet = TypeEffetBll.GetEffet("BROL"),
                    MontantTTC = 0,
                    PrixAchatTotal = 0,
                    Reste = 0 // add window for credit
                };
                EffetBll.AddEffet(_Effet);
                SharedBll.Db.SaveChanges();
            }
            SearchBox.Focus();
            if (lot != null)
                SearchBox.Text = lot.Produit.Designation;
        }
        private void AddLivraisonButtonClick(object sender, RoutedEventArgs e)
        {
            
            if (!_Effet.DetailsEffets.ToList().Any())
            {
                NotWork = true;
                DXMessageBox.Show("Ajouter des produits", "Attention", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            var client = CboxClient.SelectedItem as Data.Layers.Entities.Personne;

            try
            {
                _Effet.Personne = client;
                _Effet.Utilisateur = SharedBll.CurrentUser;
                _Effet.DateCreation = DateTime.Now;
                _Effet.TypeEffet = TypeEffetBll.GetEffet("BL");
                _Effet.MontantTTC = _Effet.DetailsEffets.ToList().Sum(o => o.TotalPrix);
                _Effet.PrixAchatTotal = _Effet.DetailsEffets.ToList().Sum(o => o.Lot.PrixAchat * o.Quantite);
                if (ActiveCredit.IsChecked == true && CreditText.Text != "")
                    _Effet.Reste = _Effet.MontantTTC - Convert.ToDouble(CreditText.Text);

                LotBll.RemoveStockClientLot(_Effet.DetailsEffets.ToList());
                SharedBll.Db.SaveChanges();

                _Effet = null;
                _Effet = new Data.Layers.Entities.Effet
                {
                    DetailsEffets = new ObservableCollection<DetailsEffet>(),
                    Utilisateur = SharedBll.CurrentUser,
                    DateCreation = DateTime.Now,
                    TypeEffet = TypeEffetBll.GetEffet("BROL"),
                    MontantTTC = 0,
                    PrixAchatTotal = 0,
                    Reste = 0 // add window for credit
                };
                EffetBll.AddEffet(_Effet);

                SharedBll.Db.SaveChanges();
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "erreur", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            NotWork = true;
            DXMessageBox.Show("le Bon de Livraison a été ajouté avec succès", "info", MessageBoxButton.OK,
                MessageBoxImage.Information);
            AddReception_OnLoaded(null, null);
            ManagGridControlSecond.ItemsSource = null;
            TotalOrderWebLbl.Content = "0";
        }

        private void Btn_addClient(object sender, RoutedEventArgs e)
        {
            try
            {
                AddClientWindow addClientWindow = new AddClientWindow();
                addClientWindow.ShowDialog();
            
                var f = SharedBll.Db.Personnes.Local.Where(c => c.TypePersonne?.Id == 1);
                if (f.Any())
                {
                    CboxClient.ItemsSource = f.ToArray();
                    CboxClient.SelectedItem = f.ToArray().LastOrDefault();
                }
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Attension", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
        }

        private void DeleteItem_Click(object sender, RoutedEventArgs e)
        {
            var detailsEffet = ManagGridControlSecond.SelectedItem as DetailsEffet;
            if (detailsEffet == null)return;
            try
            {
                DetailsEffetBll.RemoveDetails(detailsEffet);
                SharedBll.Db.SaveChanges();
                ManagGridControlSecond.ItemsSource = _Effet.DetailsEffets.ToList();
                TotalOrderWebLbl.Content = _Effet.DetailsEffets.ToList().Sum(o => o.TotalPrix) + " DA";
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Attension", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                throw;
            }
        }

        private void ManagTableView_OnCellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            try
            {
                VerifyStock(_Effet.DetailsEffets.ToList());
                ManagGridControlSecond.ItemsSource = null;
                ManagGridControlSecond.ItemsSource = _Effet.DetailsEffets.ToList();

                TotalOrderWebLbl.Content = _Effet.DetailsEffets.ToList().Sum(o => o.TotalPrix) + " DA";
                SharedBll.Db.SaveChanges();
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Attension", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }

        }

        private bool IsLotEmpty(Data.Layers.Entities.Lot lot)
        {
            if (lot?.Quantite == 0) return true;
             return false;
        }

        private bool IsAvailable(Data.Layers.Entities.Lot lot)
        {
            if (lot?.Quantite > _Effet.DetailsEffets.FirstOrDefault(e => e.Lot.Id == lot.Id)?.Quantite) return true;
             return false;
        }

        private bool IsAvailable(Data.Layers.Entities.DetailsEffet detailsEffet)
        {
            if (detailsEffet?.Quantite >= detailsEffet.Lot?.Quantite) return true;
             return false;
        }

        private void VerifyStock(List<DetailsEffet> detailsEffet)
        {
            foreach (var effet in detailsEffet)
            {
                var l = LotBll.GetById(effet.Lot.Id);
                if (effet.Quantite > l.Quantite)
                {
                    NotWork = true;
                    DXMessageBox.Show("Quantite hors Stock", "Attention", MessageBoxButton.OK, MessageBoxImage.Warning);
                    _Effet.DetailsEffets.ToList().FirstOrDefault(d => d.Lot.Id == l.Id).Quantite = l.Quantite;
                    return;
                }

            }
        }

        private void QuantityBox_OnKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                AddProductClick(sender, e);
        }

        private void ButtonRefresh_OnClick(object sender, RoutedEventArgs e)
        {
            Init();
        }

        private void AddProductClick(object sender, RoutedEventArgs e)
        {
            if (!(ManagGridControlFirst.SelectedItem is Data.Layers.Entities.Lot lot))
            {
                NotWork = true;
                DXMessageBox.Show("Selectionnez un produit", "Attention", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            if (IsLotEmpty(lot)) return;

            var isfounded = false;
            try
            {
                foreach (var item in _Effet.DetailsEffets.ToList())
                {
                    if (item.Lot.Id == lot.Id)
                    {
                        if (!IsAvailable(lot))
                        {
                            return;
                        }
                        item.Quantite += 1;
                        isfounded = true;
                        break;
                    }
                }
                if (!isfounded)
                {
                    _Effet.DetailsEffets.Add(new DetailsEffet
                    {
                        Effet = _Effet,
                        Lot = lot,
                        PrixUnitairePaye = lot.PrixVente,
                        Quantite = 1
                    });
                }

                TotalOrderWebLbl.Content = _Effet.DetailsEffets.ToList().Sum(o => o.TotalPrix) + " DA";
                Init();
                SharedBll.Db.SaveChanges();
                SearchBox.Text = "";
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Attension", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
        }
        private void AddProductKeyClick()
        {
            try
            {
                var lot = _lots.Where(c =>
                    (c.Produit?.Designation != null &&
                     c.Produit.Designation.ToLower().Contains(SearchBox.Text.ToLower())) ||
                    (c.CodeBar != null && c.CodeBar.ToLower().Contains(SearchBox.Text.ToLower()))).FirstOrDefault();

                if (IsLotEmpty(lot) || lot == null) return;

                var isfounded = false;
                foreach (var item in _Effet.DetailsEffets.ToList())
                {
                    if (item.Lot.Id == lot.Id)
                    {
                        if (!IsAvailable(lot))
                        {
                            return;
                        }
                        item.Quantite += 1;
                        isfounded = true;
                        break;
                    }
                }
                if (!isfounded)
                {
                    _Effet.DetailsEffets.Add(new DetailsEffet
                    {
                        Effet = _Effet,
                        Lot = lot,
                        PrixUnitairePaye = lot.PrixVente,
                        Quantite = 1
                    });
                }

                TotalOrderWebLbl.Content = _Effet.DetailsEffets.ToList().Sum(o => o.TotalPrix) + " DA";
                Init();
                SharedBll.Db.SaveChanges();
                SearchBox.Text = "";
            }
            catch (Exception e)
            {
                DXMessageBox.Show(e.Message, "Attension", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
        }
        private void Init()
        {
            ManagGridControlSecond.ItemsSource = null;
            ManagGridControlSecond.ItemsSource = _Effet.DetailsEffets;
        }

        private void AddReception_OnLoaded(object sender, RoutedEventArgs e)
        {
            try
            {
                _lots = null;
                _lots = new ObservableCollection<Data.Layers.Entities.Lot>(SharedBll.Db.Lots.Local);
                ManagGridControlFirst.ItemsSource = null;
                CboxClient.ItemsSource = null;
                ManagGridControlFirst.ItemsSource = _lots;
            
                var f = SharedBll.Db.Personnes.Local.Where(c => c.TypePersonne?.Id == 1);
                if (f.Any())
                {
                    CboxClient.ItemsSource = f.ToArray();
                }

                ManagGridControlSecond.ItemsSource = null;
                ManagGridControlSecond.ItemsSource = _Effet.DetailsEffets;
                TotalOrderWebLbl.Content = _Effet.DetailsEffets.ToList().Sum(o => o.TotalPrix) + " DA";
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Attension", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
        }

        private void SearchBox_OnKeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (workflow == 1)
                {
                    ManagTableViewSecond.ClearSelection();
                    ManagTableViewSecond.FocusedRowHandle = (_Effet.DetailsEffets.ToList().Count - 1);
                    ManagTableViewSecond.SelectRow(1);
                    ManagTableViewSecond.FocusedColumn = ManagTableViewSecond.VisibleColumns[1];
                    ManagTableViewSecond.FocusedRowHandle = (_Effet.DetailsEffets.ToList().Count - 1);
                    ManagTableViewSecond.ShowEditor();
                    workflow = 0;
                }
                else
                {
                    Search(SearchBox.Text.Trim() != String.Empty);
                    if (e.Key == Key.Enter)
                    {
                        if (NotWork)
                        {
                            NotWork = false;
                            return;
                        }
                        AddProductKeyClick();
                        workflow = 1;
                    }

                }
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Attension", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }

        }
        private void Montant_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex _regex = new Regex("[^0-9]+");
            e.Handled = _regex.IsMatch(e.Text);
        }
        private void Search(bool keyUpEmptyText)
        {
            ManagGridControlFirst.ItemsSource = null;

            if (SearchBox.Text.Trim() == String.Empty && keyUpEmptyText)
            {
                ManagGridControlFirst.ItemsSource = _lots;
                return;
            }

            ManagGridControlFirst.ItemsSource = null;
            ManagGridControlFirst.ItemsSource = _lots.Where(c => (c.Produit?.Designation != null && c.Produit.Designation.ToLower().Contains(SearchBox.Text.ToLower())) ||
                                                                        (c.CodeBar != null && c.CodeBar.ToLower().Contains(SearchBox.Text.ToLower()))

            );
        }

        private void AddLivraisonUc_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == (Key.F1)) SearchBox.Focus();
            if (e.Key == (Key.F2)) AddLivraisonButtonClick(null, null);
        }

        private void ActiveCredit_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ActiveCredit.IsChecked==true) CreditText.Visibility = Visibility.Visible;
                else CreditText.Visibility = Visibility.Hidden;
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Attension", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
        }
    }
}

