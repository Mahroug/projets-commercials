﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Data;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Grid;
using ProGestion.Data.Bll;
using ProGestion.Data.Layers.Entities;

namespace ProGestion.Shell.Views.Effet.Livraison
{
    /// <summary>
    /// Logique d'interaction pour EditRetourClientWindow.xaml
    /// </summary>
    public partial class EditRetourClientWindow 
    {
        private readonly ObservableCollection<DetailsEffet> _detailEffet = new ObservableCollection<DetailsEffet>();
        private Data.Layers.Entities.Effet EffetGlobale = new Data.Layers.Entities.Effet();
        private Data.Layers.Entities.Effet EffetGlobaleTracker = new Data.Layers.Entities.Effet();
        private readonly ObservableCollection<DetailsEffet> _detailEffetFirste = new ObservableCollection<DetailsEffet>();
        private Data.Layers.Entities.Effet _Effet;

        public EditRetourClientWindow()
        {
            InitializeComponent();
        }
        public EditRetourClientWindow(Data.Layers.Entities.Effet effet)
        {
            InitializeComponent();
            ManagTableViewSecond.BestFitColumns();
            ManagGridControlSecond.TotalSummary.Add(SummaryItemType.Count, "Id");
            EffetGlobaleTracker = effet;


            this.EffetGlobale = new Data.Layers.Entities.Effet()
            {
                MontantTTC = effet.MontantTTC,
                DateCreation = effet.DateCreation,
                Code = effet.Code,
                DerniereModification = effet.DerniereModification,
                DernierUtilisateur = effet.DernierUtilisateur,
                EtatEffet = effet.EtatEffet,
                Parent = effet?.Parent,
                Personne = effet.Personne,
                TypeEffet = effet.TypeEffet,
                Utilisateur = effet.Utilisateur,
                Reste = effet.MontantTTC,
            };

            foreach (var e in effet.DetailsEffets)
            {
                _detailEffetFirste.Add(new Data.Layers.Entities.DetailsEffet()
                {
                    Code = e.Code,
                    Lot = e.Lot,
                    PrixUnitairePaye = e.PrixUnitairePaye,
                    Quantite = 0,
                    Effet = EffetGlobale
                });
            }

            this.EffetGlobale.DetailsEffets = _detailEffetFirste;
        }
        private void AddRetourButtonClick(object sender, RoutedEventArgs e)
        {

            if (!EffetGlobale.DetailsEffets.ToList().Any())
            {
                DXMessageBox.Show("Il y'a aucun produit", "Attention", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }


            try
            {
                _Effet = new Data.Layers.Entities.Effet
                {
                    Personne = EffetGlobale.Personne,
                    DetailsEffets = EffetGlobale.DetailsEffets.ToList(),
                    Utilisateur = SharedBll.CurrentUser,
                    DateCreation = DateTime.Now,
                    Parent = EffetGlobaleTracker,
                    TypeEffet = TypeEffetBll.GetBRC(),
                    MontantTTC = EffetGlobale.DetailsEffets.Sum(o => o.TotalPrix),
                    PrixAchatTotal = EffetGlobale.DetailsEffets.Sum(o => o.Lot.PrixAchat * o.Quantite),
                    Reste = 0 // add window for credit
                };
                foreach (var detailsEffet in _Effet.DetailsEffets) detailsEffet.Effet = null;

                //
                LotBll.RemoveStockClientLot(DetailsEffetBll.GetByIdEffet(EffetGlobaleTracker.Id));
                var listtraker = EffetGlobaleTracker.DetailsEffets;
                var listEffet = EffetGlobale.DetailsEffets;
                foreach (var detailsEffet in listtraker)
                {
                    foreach (var effet in listEffet)
                    {
                        if (detailsEffet.Lot.Id == effet.Lot.Id)
                        {
                            detailsEffet.Quantite = effet.Quantite;
                            detailsEffet.PrixUnitairePaye = effet.PrixUnitairePaye;
                        }
                    }

                }
                EffetGlobaleTracker.MontantTTC = listtraker.Sum(p => p.TotalPrix);
                EffetGlobaleTracker.PrixAchatTotal = EffetGlobale.PrixAchatTotal;
                LotBll.AddStockClientLot(EffetGlobaleTracker.DetailsEffets.ToList());

                SharedBll.Db.SaveChanges();
                
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "erreur", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            DXMessageBox.Show("le Bon de Retour a été ajouté avec succès", "info", MessageBoxButton.OK,
                MessageBoxImage.Information);
            Close();
        }
        private void ManagTableView_OnCellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            VerifyQuantiteEffet(EffetGlobale.DetailsEffets.ToList());
            Init();
        }
        private void VerifyQuantiteEffet(List<DetailsEffet> detailsEffet)
        {
            try
            {
                foreach (var detaileffet in detailsEffet)
                {
                    var detaiQ = EffetGlobale.Parent.DetailsEffets.FirstOrDefault(d => d.Lot.Id == detaileffet.Lot.Id);
                    if (detaileffet.Quantite > detaiQ?.Quantite)
                    {
                        DXMessageBox.Show("Quantite hors le Bon de Reception", "Attention", MessageBoxButton.OK, MessageBoxImage.Warning);
                        EffetGlobale.DetailsEffets.FirstOrDefault(d => d.Lot?.Id == detaiQ.Lot?.Id).Quantite =
                            detaiQ.Quantite;
                        return;
                    }
                }
            }
            catch (Exception e)
            {
                DXMessageBox.Show(e.Message, "Attension", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
        }
        private void Init()
        {
            ManagGridControlSecond.ItemsSource = null;
            ManagGridControlSecond.ItemsSource = EffetGlobale.DetailsEffets.ToList();
            TotalOrderWebLbl.Content = EffetGlobale.DetailsEffets.ToList().Sum(o => o.TotalPrix) + " DA";
        }

        private void EditRetourClientWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            try
            {
                _detailEffet.Clear();
                ManagGridControlSecond.ItemsSource = EffetGlobale.DetailsEffets.ToList();
                TotalOrderWebLbl.Content = EffetGlobale.DetailsEffets.ToList().Sum(o => o.TotalPrix) + " DA";
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Attension", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
        }
    }
}
