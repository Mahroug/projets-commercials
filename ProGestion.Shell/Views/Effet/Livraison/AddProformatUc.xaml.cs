﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DevExpress.Data;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Grid;
using ProGestion.Data.Bll;
using ProGestion.Data.Layers.Entities;
using ProGestion.Shell.Views.Client;

namespace ProGestion.Shell.Views.Effet.Livraison
{
    /// <summary>
    /// Logique d'interaction pour AddProformatUc.xaml
    /// </summary>
    public partial class AddProformatUc : UserControl
    {
        private readonly ObservableCollection<DetailsEffet> _detailEffet = new ObservableCollection<DetailsEffet>();
        private Data.Layers.Entities.Effet _Effet;
        private ObservableCollection<Data.Layers.Entities.Lot> _lots = new ObservableCollection<Data.Layers.Entities.Lot>();
        public AddProformatUc()
        {
            InitializeComponent();
            ManagTableViewFirst.BestFitColumns();
            ManagGridControlFirst.TotalSummary.Add(SummaryItemType.Count, "Id");
            ManagTableViewSecond.BestFitColumns();
            ManagGridControlSecond.TotalSummary.Add(SummaryItemType.Count, "Id");
        }
        private void AddProformatButtonClick(object sender, RoutedEventArgs e)
        {
            var client = CboxClient.SelectedItem as Data.Layers.Entities.Personne;

            if (!_detailEffet.Any())
            {
                DXMessageBox.Show("Ajouter des produits", "Attention", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            try
            {
                _Effet = new Data.Layers.Entities.Effet
                {
                    Personne = client,
                    DetailsEffets = _detailEffet.ToList(),
                    Utilisateur = SharedBll.CurrentUser,
                    DateCreation = DateTime.Now,
                    TypeEffet = TypeEffetBll.GetFP(),
                    MontantTTC = _detailEffet.Sum(o => o.TotalPrix),
                    PrixAchatTotal = _detailEffet.Sum(o => o.Lot.PrixAchat * o.Quantite),
                    Reste = 0 // add window for credit
                };
                EffetBll.AddEffet(_Effet);

                SharedBll.Db.SaveChanges();
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "erreur", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            DXMessageBox.Show("le Bon de Livraison a été ajouté avec succès", "info", MessageBoxButton.OK,
                MessageBoxImage.Information);
            AddProformat_OnLoaded(null, null);

        }

        private void Btn_addClient(object sender, RoutedEventArgs e)
        {
            try
            {
                AddClientWindow addClientWindow = new AddClientWindow();
                addClientWindow.ShowDialog();
                // Refresh_();
                var f = SharedBll.Db.Personnes.Local.Where(c => c.TypePersonne?.Id == 1);
                if (f.Any())
                {
                    CboxClient.ItemsSource = f.ToArray();
                    CboxClient.SelectedItem = f.ToArray().LastOrDefault();
                }
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Attension", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
        }

        private void DeleteItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var detailsEffet = ManagGridControlSecond.SelectedItem as DetailsEffet;
                if (detailsEffet != null)
                {
                    _detailEffet.Remove(detailsEffet);
                    TotalOrderWebLbl.Content = _detailEffet.Sum(o => o.TotalPrix) + " DA";
                }
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Attension", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
        }

        private void ManagTableView_OnCellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            VerifyStock(_detailEffet.ToList());
            ManagGridControlSecond.ItemsSource = null;
            ManagGridControlSecond.ItemsSource = _detailEffet;

            TotalOrderWebLbl.Content = _detailEffet.Sum(o => o.TotalPrix) + " DA";
        }

        private void VerifyStock(List<DetailsEffet> detailsEffet)
        {
            foreach (var effet in detailsEffet)
            {
                var l = LotBll.GetById(effet.Lot.Id);
                if (effet.Quantite > l.Quantite)
                {
                    DXMessageBox.Show("Quantite hors Stock", "Attention", MessageBoxButton.OK, MessageBoxImage.Warning);
                    _detailEffet.FirstOrDefault(d => d.Lot.Id == l.Id).Quantite = l.Quantite;
                    return;
                }

            }
        }
        private void AddProductClick(object sender, RoutedEventArgs e)
        {
            if (!(ManagGridControlFirst.SelectedItem is Data.Layers.Entities.Lot lot))

            {
                DXMessageBox.Show("Selectionnez un produit", "Attention", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            try
            {
                var isfounded = false;
                foreach (var item in _detailEffet)
                {
                    if (item.Lot.Id == lot.Id)
                    {
                        item.Quantite += 1;
                        isfounded = true;
                        break;
                    }
                }
                if (!isfounded)
                {
                    _detailEffet.Add(new DetailsEffet
                    {
                        Lot = lot,
                        PrixUnitairePaye = lot.PrixVente,
                        Quantite = 1
                    });
                }
                VerifyStock(_detailEffet.ToList());
                TotalOrderWebLbl.Content = _detailEffet.Sum(o => o.TotalPrix) + " DA";
                Init();
                SearchBox.Text = "";
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Attension", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
        }

        private void Init()
        {
            ManagGridControlSecond.ItemsSource = null;
            ManagGridControlSecond.ItemsSource = _detailEffet;
        }

        private void AddProformat_OnLoaded(object sender, RoutedEventArgs e)
        {
            try
            {
                _lots = null;
                _lots = new ObservableCollection<Data.Layers.Entities.Lot>(SharedBll.Db.Lots.Local);
                ManagGridControlFirst.ItemsSource = null;
                CboxClient.ItemsSource = null;
                ManagGridControlFirst.ItemsSource = _lots;
                _detailEffet.Clear();

                var f = SharedBll.Db.Personnes.Local.Where(c => c.TypePersonne?.Id == 1);
                if (f.Any())
                {
                    CboxClient.ItemsSource = f.ToArray();
                }
                ManagGridControlSecond.ItemsSource = _detailEffet;
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Attension", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
        }

        private void SearchBox_OnKeyUp(object sender, KeyEventArgs e)
        {
            Search(SearchBox.Text.Trim() != String.Empty);
        }
        private void Search(bool keyUpEmptyText)
        {
            ManagGridControlFirst.ItemsSource = null;

            if (SearchBox.Text.Trim() == String.Empty && keyUpEmptyText)
            {
                ManagGridControlFirst.ItemsSource = _lots;
                return;
            }

            ManagGridControlFirst.ItemsSource = null;
            ManagGridControlFirst.ItemsSource = _lots.Where(c => (c.Produit?.Designation != null && c.Produit.Designation.ToLower().Contains(SearchBox.Text.ToLower())) ||
                                                                        (c.CodeBar != null && c.CodeBar.ToLower().Contains(SearchBox.Text.ToLower()))
                                                                        

            );
        }

    }
}


    