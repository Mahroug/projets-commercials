﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.Xpf.Core;
using ProGestion.Data.Bll;
using SelectionChangedEventArgs = System.Windows.Controls.SelectionChangedEventArgs;
using UserControl = System.Windows.Controls.UserControl;

namespace ProGestion.Shell.Views.Statistique
{
    /// <summary>
    /// Logique d'interaction pour StatistiqueUc.xaml
    /// </summary>
    public partial class StatistiqueUc : UserControl
    {
         private ObservableCollection<Data.Bll.InternalClass.Recette> _manageSource = new ObservableCollection<Data.Bll.InternalClass.Recette>();
         private ObservableCollection<Data.Layers.Entities.Effet> ListBl ;
         private ObservableCollection<Data.Layers.Entities.Effet> ListBRC ;
         private ObservableCollection<Data.Layers.Entities.Effet> ListDEP ;
        public StatistiqueUc()
        {
            InitializeComponent();
            ManagTableView.BestFitColumns();
            ManagGridControl.TotalSummary.Add(SummaryItemType.Count, "Id");
            Refresh();
        }
        private void ShowWindowClick(object sender, RoutedEventArgs e)
        {
            new ShowStatiqueWindow().ShowDialog();
         
        }
        private void Refresh()
        {
            try
            {
                ManagTableView.RowMinHeight = SharedBll.UcFontSize * 2.85;
                _manageSource = new ObservableCollection<Data.Bll.InternalClass.Recette>();
                ListBl = null;
                ListBl = new ObservableCollection<Data.Layers.Entities.Effet>(SharedBll.Db.Effets.Local.Where(c => c.TypeEffet?.Abreviation == "BL").OrderBy(f=>f.DateCreation));
                ListBRC = new ObservableCollection<Data.Layers.Entities.Effet>(SharedBll.Db.Effets.Local.Where(c => c.TypeEffet?.Abreviation == "BRC").OrderBy(f=>f.DateCreation));
                ListDEP = new ObservableCollection<Data.Layers.Entities.Effet>(SharedBll.Db.Effets.Local.Where(c => c.TypeEffet?.Abreviation == "DEP").OrderBy(f=>f.DateCreation));
                var listDate = ListBl.Select(f => f.DateCreation.Date).Distinct(); 
                var listDateDep = ListDEP.Select(f => f.DateCreation.Date).Distinct();
                var List= listDate.Concat(listDateDep).Distinct();
                foreach (var date in List)
                {
                    var v = ListBl.Where(p => p.DateCreation.Date == date).Sum(o => o.MontantTTC);
                    var a = ListBl.Where(p => p.DateCreation.Date == date).Sum(o => o.PrixAchatTotal);
                    var d = ListDEP.Where(p => p.DateCreation.Date == date).Sum(o => o.MontantTTC);

                    var vretour = ListBRC.Where(p => p.DateCreation.Date == date).Sum(o => o.MontantTTC);
                    var aretour = ListBRC.Where(p => p.DateCreation.Date == date).Sum(o => o.PrixAchatTotal);
                   
                    _manageSource.Add(new InternalClass.Recette()
                        {
                            MontantV = v- vretour,
                            Date =date,
                            MontantA = a- aretour,
                            Depense = d
                    });
                }
                ManagGridControl.ItemsSource = _manageSource;
            }

            catch (Exception ex)
            {
                DXMessageBox.Show(ex.Message, "error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SearchBox_OnKeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            ManagGridControl.ItemsSource = null;

            ManagGridControl.ItemsSource = _manageSource.Where(c => c.Date.ToString().Contains(SearchBox.Text));
        }

        private void SearchBox_OnSelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            SearchBox_OnKeyUp(null,null);
        }
    }
}
