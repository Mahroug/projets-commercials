﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ProGestion.Data.Bll;

namespace ProGestion.Shell.Views.Statistique
{
    /// <summary>
    /// Logique d'interaction pour ShowStatiqueWindow.xaml
    /// </summary>
    public partial class ShowStatiqueWindow 
    {
        private ObservableCollection<Data.Layers.Entities.Effet> ListBl;
        private ObservableCollection<Data.Layers.Entities.Effet> ListBRC;
        public ShowStatiqueWindow()
        {
            InitializeComponent();
            ChiffreLabel.Content = SharedBll.Db.Lots.Local.Sum(l => l.PrixAchat * l.Quantite) + " DA";
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            if (Debut.Text == "")
            {
                Debut.Focus();
                return ;
            }
            if (Fin.Text == "")
            {
                Fin.Focus();
                return;
            }
            var datedebut = Debut.SelectedDate;
            var datefin = Fin.SelectedDate;
            ListBl = null;
            ListBl = new ObservableCollection<Data.Layers.Entities.Effet>(SharedBll.Db.Effets.Local.Where(c => c.TypeEffet?.Abreviation == "BL").OrderBy(f => f.DateCreation));
            ListBRC = new ObservableCollection<Data.Layers.Entities.Effet>(SharedBll.Db.Effets.Local.Where(c => c.TypeEffet?.Abreviation == "BRC").OrderBy(f => f.DateCreation));
            var listDate = ListBl.Select(f => f.DateCreation.Date).Distinct();
            listDate = listDate.Where(d => d > datedebut && d < datefin);
            double totalv = 0;
            double totala = 0;
            foreach (var date in listDate)
            {
                var v = ListBl.Where(p => p.DateCreation.Date == date).Sum(o => o.MontantTTC);
                var a = ListBl.Where(p => p.DateCreation.Date == date).Sum(o => o.PrixAchatTotal);

                var vretour = ListBRC.Where(p => p.DateCreation.Date == date).Sum(o => o.MontantTTC);
                var aretour = ListBRC.Where(p => p.DateCreation.Date == date).Sum(o => o.PrixAchatTotal);
                totalv += v - vretour;
                totala += a - aretour;
            }
            var recette = new InternalClass.Recette()
            {
                MontantA = totala,
                MontantV = totalv
            };
            MontantLabel.Content = "Montant : "+ recette.MontantV + " DA" ;
            BeneficeLabel.Content = "Benefice : "+ recette.Benefice + " DA";
        }
    }
}
