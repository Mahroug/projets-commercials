﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.Xpf.Core;
using ProGestion.Data.Bll;
using ProGestion.Shell.Views.CategorieProduit;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using UserControl = System.Windows.Controls.UserControl;

namespace ProGestion.Shell.Views.CategorieProduit
{
    /// <summary>
    /// Logique d'interaction pour CategorieProduit.xaml
    /// </summary>
    public partial class CategorieProduitUc 
    {
        private ObservableCollection<Data.Layers.Entities.CategorieProduit> _manageSource =
            new ObservableCollection<Data.Layers.Entities.CategorieProduit>();
        public CategorieProduitUc()
        {
            InitializeComponent();
            Refresh();
            ManagTableView.BestFitColumns();
            ManagGridControl.TotalSummary.Add(SummaryItemType.Count, "Id");
        }

        private void Refresh()
        {
            //change font size
            ManagGridControl.FontSize = SharedBll.UcFontSize;

            _manageSource = null;
            _manageSource = new ObservableCollection<Data.Layers.Entities.CategorieProduit>(SharedBll.Db.CategorieProduits.Local);

            if (!_manageSource.Any()) return;
            try
            {
                ManagGridControl.ItemsSource = null;
                ManagTableView.RowMinHeight = SharedBll.UcFontSize * 2.85;
                Search(true);

            }
            catch (Exception e)
            {
                DXMessageBox.Show(e.Message, "error", MessageBoxButton.OK, MessageBoxImage.Error);

            }
        }
       
        private void Search(bool keyUpEmptyText)
        {
            ManagGridControl.ItemsSource = null;
            try
            {

                if (SearchBox.Text.Trim() == String.Empty && keyUpEmptyText)
                {
                    ManagGridControl.ItemsSource =
                        new ObservableCollection<Data.Layers.Entities.CategorieProduit>(SharedBll.Db.CategorieProduits.Local);
                    return;
                }

                ManagGridControl.ItemsSource = null;
                ManagGridControl.ItemsSource = _manageSource.Where(c =>
                    (c.Designation != null &&
                     c.Designation.ToLower().Contains(SearchBox.Text.ToLower()))
                );
            }
            catch (Exception e)
            {
                DXMessageBox.Show(e.Message, "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void Excel_OnClick(object sender, RoutedEventArgs e)
        {

            try
            {
                var saveFileDialog = new SaveFileDialog
                {
                    InitialDirectory = Convert.ToString(Environment.SpecialFolder.Desktop),
                    Filter = @"(*.XLSX)|*.xlsx|All Files (*.*)|*.*",
                    FilterIndex = 1,
                    FileName = "List"
                };

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                    ManagTableView.ExportToXlsx(saveFileDialog.FileName);
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);

            }

        }
        private void DeleteCategorieProduitClick(object sender, RoutedEventArgs e)
        {
            if (ManagGridControl.SelectedItem == null) return;

            try
            {
                var categorieProduit = ManagGridControl.SelectedItem as Data.Layers.Entities.CategorieProduit;
                var result = DXMessageBox.Show("Voulez vous supprimer cette ligne?", "Info", MessageBoxButton.YesNo,
                    MessageBoxImage.Warning);
                if (result != MessageBoxResult.Yes) return;

                CategorieProduitBll.RemoveCategoryProduit(categorieProduit);
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            SharedBll.Db.SaveChanges();
            DXMessageBox.Show("suppression avec succès", "info", MessageBoxButton.OK, MessageBoxImage.Information);
            Search(true);
        }

        private void EditCategorieProduitClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var category = ManagGridControl.SelectedItem as Data.Layers.Entities.CategorieProduit;
                if (category == null) return;

                new EditCategorieProduitWindow(category).ShowDialog();
                Refresh();
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddCategorieProduitClick(object sender, RoutedEventArgs e)
        {
            new AddCategorieProduitWindow().ShowDialog();
            Refresh();
        }

        private void SearchBox_OnKeyUp(object sender, KeyEventArgs e)
        {
            Search(SearchBox.Text.Trim() != String.Empty);
        }
    }
}
