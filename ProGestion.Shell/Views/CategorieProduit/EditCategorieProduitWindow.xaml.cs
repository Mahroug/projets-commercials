﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Xpf.Core;
using ProGestion.Data.Bll;
using ProGestion.Data.Layers.Entities;

namespace ProGestion.Shell.Views.CategorieProduit
{
    /// <summary>
    /// Logique d'interaction pour EditCategorieProduitWindow.xaml
    /// </summary>
    public partial class EditCategorieProduitWindow 
    {
        public  Data.Layers.Entities.CategorieProduit  Category = new Data.Layers.Entities.CategorieProduit();
        public EditCategorieProduitWindow()
        {
            InitializeComponent();
        }

        public EditCategorieProduitWindow(Data.Layers.Entities.CategorieProduit category)
        {
            InitializeComponent();
            this.Category = category;
            FillData();
        }

        private void FillData()
        {
            Categorie.SelectedItem = Category.Parent;
            Designation.Text = Category.Designation;
        }
        private void EditCategorieProduitWindow_OnLoadedCategorieProduitWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            Categorie.ItemsSource = SharedBll.Db.CategorieProduits?.ToArray();
        }

        private void SaveClick(object sender, RoutedEventArgs e)
        {
            if (!VerifyFieldsFields()) return;
            EditCategorieProduit.IsEnabled = false;
            try
            {

                Category.Parent = Categorie.SelectionBoxItem as Data.Layers.Entities.CategorieProduit;
                Category.Designation = Designation.Text;
                CategorieProduitBll.UpdateCategorieProduit(Category);
                SharedBll.Db.SaveChanges();

                EditCategorieProduit.IsEnabled = true;
                Close();
                DXMessageBox.Show("Flexy Modifié avec succès", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private bool VerifyFieldsFields()
        {
           if (Designation.Text == "")
            {
                Designation.Focus();
                return false;
            }
            return true;
        }
    }
}
