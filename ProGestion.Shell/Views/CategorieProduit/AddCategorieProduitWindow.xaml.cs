﻿using System;
using System.Linq;
using System.Windows;
using DevExpress.Xpf.Core;
using ProGestion.Data.Bll;

namespace ProGestion.Shell.Views.CategorieProduit
{
    public partial class AddCategorieProduitWindow
    {
        public AddCategorieProduitWindow()
        {
            InitializeComponent();
        }

        private void AddCategorieProduitWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            Categorie.ItemsSource = SharedBll.Db.CategorieProduits?.ToArray();
        }

        private void SaveClick(object sender, RoutedEventArgs e)
        {
            ///// verify fields
            if (!VerifyFieldsFields()) return;
            AddCategorieProduit.IsEnabled = false;
            try
            {
                var categorieProduit = new Data.Layers.Entities.CategorieProduit
                {
                    Parent = Categorie.SelectedItem as Data.Layers.Entities.CategorieProduit,
                    Designation = Designation.Text
                };
                CategorieProduitBll.AddCategorieProduit(categorieProduit);
                SharedBll.Db.SaveChanges();

                AddCategorieProduit.IsEnabled = true;
                Close();
                DXMessageBox.Show("Categorié de Produit ajouté avec succès", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }
        private bool VerifyFieldsFields()
        {
            if (Designation.Text == "")
            {
                Designation.Focus();
                return false;
            }
            return true;
        }
    }
}
