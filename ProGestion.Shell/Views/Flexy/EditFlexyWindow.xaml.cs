﻿using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;
using DevExpress.Xpf.Core;
using ProGestion.Data.Bll;
using ProGestion.Data.Layers.Entities;

namespace ProGestion.Shell.Views.Flexy
{
    /// <summary>
    /// Logique d'interaction pour EditFlexyWindow.xaml
    /// </summary>
    public partial class EditFlexyWindow 
    {
        public Data.Layers.Entities.Flexy Flexy = new Data.Layers.Entities.Flexy();
        public EditFlexyWindow(Data.Layers.Entities.Flexy flexy)
        {
            InitializeComponent();
            this.Flexy = flexy;
            FillData();
        }
        private void EditFlexyWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            Operateur.ItemsSource = SharedBll.Db.Operateurs.Local.ToArray();
            Personne.ItemsSource = SharedBll.Db.Personnes.Local.Where(p => p.TypePersonne.Id == 1).ToArray();
        }

        private void Montant_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex _regex = new Regex("[^0-9]+");
            e.Handled = _regex.IsMatch(e.Text);
        }

        private void FillData()
        {
            Personne.SelectedItem = Flexy.Personne;
            Operateur.SelectedItem = Flexy.Operateur;
            Montant.Text = Flexy.Montant.ToString(CultureInfo.InvariantCulture);
            Versement.Text = Flexy.Versement.ToString(CultureInfo.InvariantCulture);
        }

        private void SaveClick(object sender, RoutedEventArgs e)
        {
            if (!VerifyFieldsFields()) return;
            EditFlexy.IsEnabled = false;
            try
            {

                Flexy.Operateur = Operateur.SelectedItem as Operateur;
                Flexy.Personne = Personne.SelectedItem as Personne;
                Flexy.Versement = Convert.ToDouble(Versement.Text);
                Flexy.Montant = Convert.ToDouble(Montant.Text);
                Flexy.DerniereModification = DateTime.Now;
                Flexy.DernierUtilisateur = SharedBll.CurrentUser;
                
                FlexyBll.UpdateFlexy(Flexy);
                SharedBll.Db.SaveChanges();

                EditFlexy.IsEnabled = true;
                Close();
                DXMessageBox.Show("Flexy Modifié avec succès", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private bool VerifyFieldsFields()
        {
            if (!(Personne.SelectedItem is Personne))
            {
                Personne.Focus();
                return false;
            }

            if (!(Operateur.SelectedItem is Operateur))
            {
                Operateur.Focus();
                return false;
            }

            if (Montant.Text == "")
            {
                Montant.Focus();
                return false;
            }

            if (Versement.Text == "")
            {
                Versement.Focus();
                return false;
            }

            return true;
        }
    }
}
