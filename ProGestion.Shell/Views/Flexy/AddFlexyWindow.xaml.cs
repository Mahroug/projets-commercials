﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;
using DevExpress.Xpf.Core;
using ProGestion.Data.Bll;
using ProGestion.Data.Layers.Entities;

namespace ProGestion.Shell.Views.Flexy
{
     public partial class AddFlexyWindow
    {
        public AddFlexyWindow()
        {
            InitializeComponent();
        }

        private void AddFlexyWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            try
            {
                var o = SharedBll.Db.Operateurs.Local.ToArray();
                var c = SharedBll.Db.Personnes.Local.Where(cc => cc.TypePersonne?.Id == 2 || cc.TypePersonne?.Id==1);
                if (c.Any())
                {
                    Personne.ItemsSource = c.ToArray();
                }
                if (o.Any())
                {
                    Operateur.ItemsSource = o.ToArray();
                }
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
          
        }

        private void Montant_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex _regex = new Regex("[^0-9]+");
            e.Handled = _regex.IsMatch(e.Text);
        }

        private void SaveClick(object sender, RoutedEventArgs e)
        {
            ///// verify fields
            if (!VerifyFieldsFields()) return;
            AddFlexy.IsEnabled = false;
            try
            {
                var flexy = new Data.Layers.Entities.Flexy
                {
                    Operateur = Operateur.SelectedItem as Operateur,
                    Personne = Personne.SelectedItem as Data.Layers.Entities.Personne,
                    Versement = Convert.ToDouble(Versement.Text),
                    Montant = Convert.ToDouble(Montant.Text),
                    DateCreation = DateTime.Now,
                    Utilisateur = SharedBll.CurrentUser
                };
                FlexyBll.AddFlexy(flexy);
                SharedBll.Db.SaveChanges();

                AddFlexy.IsEnabled = true;
                Close();
                DXMessageBox.Show("Flexy ajouté avec succès", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }
        private bool VerifyFieldsFields()
        {
            if (!(Personne.SelectedItem is Data.Layers.Entities.Personne))
            {
                Personne.Focus();
                return false;
            }

            if (!(Operateur.SelectedItem is Data.Layers.Entities.Operateur))
            {
                Operateur.Focus();
                return false;
            }

            if (Montant.Text == "")
            {
                Montant.Focus();
                return false;
            }

            if (Versement.Text == "")
            {
                Versement.Focus();
                return false;
            }

            return true;
        }
    }
}
