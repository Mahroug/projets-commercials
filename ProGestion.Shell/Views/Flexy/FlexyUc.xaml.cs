﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.Xpf.Core;
using ProGestion.Data.Bll;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using UserControl = System.Windows.Controls.UserControl;

namespace ProGestion.Shell.Views.Flexy
{
    /// <summary>
    /// Logique d'interaction pour FlexyUc.xaml
    /// </summary>
    public partial class FlexyUc 
    {
        private ObservableCollection<Data.Layers.Entities.Flexy> _manageSource =
            new ObservableCollection<Data.Layers.Entities.Flexy>();

        public FlexyUc()
        {
            InitializeComponent();
            Refresh();
            ManagTableView.BestFitColumns();
            ManagGridControl.TotalSummary.Add(SummaryItemType.Count, "Id");
        }

        private void Refresh()
        {
            //change font size
            ManagGridControl.FontSize = SharedBll.UcFontSize;

            _manageSource = null;
            _manageSource = new ObservableCollection<Data.Layers.Entities.Flexy>(SharedBll.Db.Flexies.Local);

            if (!_manageSource.Any()) return;
            try
            {
                ManagGridControl.ItemsSource = null;
                ManagTableView.RowMinHeight = SharedBll.UcFontSize * 2.85;
                Search(true);

            }
            catch (Exception e)
            {
                DXMessageBox.Show(e.Message, "error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddFlexyClick(object sender, RoutedEventArgs e)
        {
            new AddFlexyWindow().ShowDialog();
            Refresh();
        }

        private void SearchBox_OnKeyUp(object sender, KeyEventArgs e)
        {
            Search(SearchBox.Text.Trim() != String.Empty);
        }

        private void Search(bool keyUpEmptyText)
        {
            ManagGridControl.ItemsSource = null;

            try
            {
                if (SearchBox.Text.Trim() == String.Empty && keyUpEmptyText)
                {
                    ManagGridControl.ItemsSource =
                        new ObservableCollection<Data.Layers.Entities.Flexy>(SharedBll.Db.Flexies.Local);
                    return;
                }

                ManagGridControl.ItemsSource = null;
                ManagGridControl.ItemsSource = _manageSource.Where(c =>
                    (c.Operateur?.Designation != null &&
                     c.Operateur.Designation.ToLower().Contains(SearchBox.Text.ToLower())) ||
                    (c.Personne.FullName != null && c.Personne.FullName.ToLower().Contains(SearchBox.Text.ToLower())) ||
                    (c.Montant.ToString(CultureInfo.InvariantCulture).Contains(SearchBox.Text.ToLower())) ||
                    (c.Versement.ToString(CultureInfo.InvariantCulture).Contains(SearchBox.Text.ToLower()))
                );
            }
            catch (Exception e)
            {
                DXMessageBox.Show(e.Message, "error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EditFlexyClick(object sender, RoutedEventArgs e)
        {
            var flexy = ManagGridControl.SelectedItem as Data.Layers.Entities.Flexy;
            if (flexy == null) return;

            new EditFlexyWindow(flexy).ShowDialog();
            Refresh();
        }

        private void DeleteFlexyClick(object sender, RoutedEventArgs e)
        {
            if (ManagGridControl.SelectedItem == null) return;

            Data.Layers.Entities.Flexy flexy;
            try
            {
                flexy = ManagGridControl.SelectedItem as Data.Layers.Entities.Flexy;
                var result = DXMessageBox.Show("Voulez vous supprimer cette ligne?", "Info", MessageBoxButton.YesNo,
                    MessageBoxImage.Warning);
                if (result != MessageBoxResult.Yes) return;

                FlexyBll.RemoveFlexy(flexy);
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            SharedBll.Db.SaveChanges();
            DXMessageBox.Show("suppression avec succès", "info", MessageBoxButton.OK, MessageBoxImage.Information);
            Search(true);
        }

        private void Excel_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var saveFileDialog = new SaveFileDialog
                {
                    InitialDirectory = Convert.ToString(Environment.SpecialFolder.Desktop),
                    Filter = @"(*.XLSX)|*.xlsx|All Files (*.*)|*.*",
                    FilterIndex = 1,
                    FileName = "List"
                };

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                    ManagTableView.ExportToXlsx(saveFileDialog.FileName);
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            
        }

    }
}
