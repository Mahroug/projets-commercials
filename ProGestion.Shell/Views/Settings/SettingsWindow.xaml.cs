﻿using System.Windows;
using ProGestion.Data.Bll;

namespace ProGestion.Shell.Views.Settings
{
    /// <summary>
    /// Interaction logic for SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow
    {
        public static string DefaultPrinterCombobox;
        public SettingsWindow()
        {
            InitializeComponent();
            if (SharedBll.CurrentUser.Role.Designation != "administrateur")
            {
                BddI.Visibility = Visibility.Hidden;
            }
        }

        public void LoadData()
        {
            DefaultPrinterCombobox = SharedBll.DefaultPrinter;
        }


        private void PrintItem(object sender, RoutedEventArgs e)
        {
            SettingsNavigationFrame.Navigate(typeof(Imprimante.ImprimanteSettingsUc));
        }

        private void UtilisateurItem(object sender, RoutedEventArgs e)
        {
            SettingsNavigationFrame.Navigate(typeof(Utilisateur.UtilisateurSettingsUc));
        }

        private void BddItem(object sender, RoutedEventArgs e)
        {
            SettingsNavigationFrame.Navigate(typeof(BDD.BackUpUc));
        }
    }
}
 