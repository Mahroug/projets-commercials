﻿using System;
using System.Windows;
using System.Windows.Forms;
using DevExpress.Xpf.Core;
using ProGestion.Data.Bll;
using UserControl = System.Windows.Controls.UserControl;

namespace ProGestion.Shell.Views.Settings.BDD
{
    public partial class BackUpUc : UserControl
    {
        private string sSelectedPath = "";
        public BackUpUc()
        {
            InitializeComponent();
        }

        private void DatabasePath_OnClick(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.Description = "Custom Description";

            if (fbd.ShowDialog() == DialogResult.OK)
            {
                sSelectedPath = fbd.SelectedPath;
                DbTextBox.Text = sSelectedPath;
            }
        }

        private void Save_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (sSelectedPath.Equals(""))
                    return;
                ProGestionBll.BackUp(sSelectedPath);
                DXMessageBox.Show("La Base de Données a été Sauvgardée avec succés", "Info", MessageBoxButton.OK,
                    MessageBoxImage.Information);
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message,"erreur", MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }

        }
    }
}
