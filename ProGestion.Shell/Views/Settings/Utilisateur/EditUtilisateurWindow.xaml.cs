﻿using System;
using System.Linq;
using System.Windows;
using DevExpress.Xpf.Core;
using ProGestion.Data.Bll;
using ProGestion.Resources;

namespace ProGestion.Shell.Views.Settings.Utilisateur
{
    /// <summary>
    /// Interaction logic for EditUtilisateurWindow.xaml
    /// </summary>
    public partial class EditUtilisateurWindow
    {
        public Data.Layers.Entities.Utilisateur _Utilisateur;
        public EditUtilisateurWindow(Data.Layers.Entities.Utilisateur user)
        {
            InitializeComponent();
            if (SharedBll.CurrentUser.Id == user.Id)
            {
                Role.IsEnabled = false;
            }
            _Utilisateur = user;
            FillData();
        }

        private void FillData()
        {
            Role.ItemsSource = SharedBll.Db.Roles.Local.ToArray();
            Role.SelectedItem = _Utilisateur.Role;
            Username.Text = _Utilisateur.Username;
        }

        private void SaveClick(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(Username.Text))
                _Utilisateur.Username = Username.Text;

            _Utilisateur.Role = Role.SelectedItem as Data.Layers.Entities.Role;

            if ((SharedBll.CurrentUser.Role.Designation == "administrateur" && SharedBll.CurrentUser.Id != _Utilisateur.Id) &&
                (!string.IsNullOrEmpty(NewUserPassword.Password) || !string.IsNullOrEmpty(ConfirmationNewUserPassword.Password)))
            {
                if (NewUserPassword.Password == "")
                {
                    NewUserPassword.Focus();
                    return;
                }
                if (ConfirmationNewUserPassword.Password == "")
                {
                    NewUserPassword.Focus();
                    return;
                }
                if (NewUserPassword.Password != ConfirmationNewUserPassword.Password)
                {
                    PasswordsNotSame.Visibility = Visibility.Visible;
                    ConfirmationNewUserPassword.Focus();
                    return;
                }
                _Utilisateur.Password = Tools.CryptedPassword(NewUserPassword.Password);
            } else if (!string.IsNullOrEmpty(OldUserPassword.Password) || !string.IsNullOrEmpty(NewUserPassword.Password) ||
                !string.IsNullOrEmpty(ConfirmationNewUserPassword.Password))
            {
                if (OldUserPassword.IsEnabled && OldUserPassword.Password == "")
                {
                    OldUserPassword.Focus();
                    return;
                }
                if (NewUserPassword.Password == "")
                {
                    NewUserPassword.Focus();
                    return;
                }
                if (ConfirmationNewUserPassword.Password == "")
                {
                    NewUserPassword.Focus();
                    return;
                }
                if (NewUserPassword.Password != ConfirmationNewUserPassword.Password)
                {
                    PasswordsNotSame.Visibility = Visibility.Visible;
                    ConfirmationNewUserPassword.Focus();
                    return;
                }
                //if (!SharedBll.CurrentUser.Role || SharedBll.CurrentUser.Id == _Utilisateur.Id)
                if (_Utilisateur.Password != Tools.CryptedPassword(OldUserPassword.Password))
                {
                    PasswordErr.Visibility = Visibility.Visible;
                    OldUserPassword.Focus();
                    return;
                }

                //isPWDModified = true;
                _Utilisateur.Password = Tools.CryptedPassword(NewUserPassword.Password);
            }
            else
            {
                UtilisateurBll.UpdateUser(_Utilisateur);

                SharedBll.Db.SaveChanges();
                Close();
                DXMessageBox.Show("L'utilisateur a été modifié avec succès", "info", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            try
            {
                UtilisateurBll.UpdateUser(_Utilisateur);
                SharedBll.Db.SaveChanges();
                Close();
                DXMessageBox.Show("L'utilisateur a été modifié avec succès", "info", MessageBoxButton.OK, MessageBoxImage.Information);
            }

            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
