﻿using System;
using System.Linq;
using System.Windows;
using DevExpress.Xpf.Core;
using ProGestion.Data.Bll;
using ProGestion.Data.Layers.Entities;
using ProGestion.Resources;
using ProGestion.Shell.Views.Settings.Utilisateur;

namespace ProGestion.Shell.Views.Settings.Utilisateur
{
    /// <summary>
    ///     Interaction logic for AddBrandsWindow.xaml
    /// </summary>
    public partial class AjouterUtilisateurWindow
    {
        public Data.Layers.Entities.Utilisateur user;

        public AjouterUtilisateurWindow()
        {
            InitializeComponent();
            Role.ItemsSource = SharedBll.Db.Roles.Local.ToArray();
        }

        private void SaveClick(object sender, RoutedEventArgs e)
        {
            if (UserUsername.Text == "")
            {
                UserUsername.Focus();
                return;
            }
            if (PasswordBoxUserPassword.Password == "")
            {
                PasswordBoxUserPassword.Focus();
                return;
            }
            if (PasswordBoxUserPasswordConfirmation.Password == "")
            {
                PasswordBoxUserPasswordConfirmation.Focus();
                return;
            }
            if (PasswordBoxUserPassword.Password != PasswordBoxUserPasswordConfirmation.Password)
            {
                PasswordErrLabel.Visibility = Visibility.Visible;
                PasswordBoxUserPasswordConfirmation.Focus();
                return;
            }
            try
            {
                var user = new Data.Layers.Entities.Utilisateur
                {
                    Username = UserUsername.Text,
                    Role = Role.SelectedItem as Role,
                    Password = Tools.CryptedPassword(PasswordBoxUserPasswordConfirmation.Password)
                };
                UtilisateurBll.AddUser(user);

                SharedBll.Db.SaveChanges();

                Close();
                DXMessageBox.Show("Utilisateur ajouté avec succès", "info", MessageBoxButton.OK,
                    MessageBoxImage.Information);
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
