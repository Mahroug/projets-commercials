﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using DevExpress.Xpf.Core;
using ProGestion.Data.Bll;

namespace ProGestion.Shell.Views.Settings.Utilisateur
{
    /// <summary>
    /// Interaction logic for UtilisateurSettingsUc.xaml
    /// </summary>
    public partial class UtilisateurSettingsUc : UserControl
    {
        private ObservableCollection<Data.Layers.Entities.Utilisateur> _manageSource = new ObservableCollection<Data.Layers.Entities.Utilisateur>();

        public UtilisateurSettingsUc()
        {
            InitializeComponent();
            Refresh();
        }

        public void Refresh()
        {
            _manageSource = null;
            if(SharedBll.CurrentUser.Role.Designation != "administrateur")
                _manageSource = new ObservableCollection<Data.Layers.Entities.Utilisateur>(SharedBll.Db.Utilisateurs.Where(u => u.Id == SharedBll.CurrentUser.Id));
            else
                _manageSource = new ObservableCollection<Data.Layers.Entities.Utilisateur>(SharedBll.Db.Utilisateurs);

            if (!_manageSource.Any()) return;
            ManagGridControl.ItemsSource = _manageSource;
        }

        private void Btn_AddUser(object sender, RoutedEventArgs e)
        {
            var addUserWindow = new AjouterUtilisateurWindow();
            addUserWindow.ShowDialog();
            Refresh();
        }

        private void EditUserClick(object sender, RoutedEventArgs e)
        {
            var user = ManagGridControl.SelectedItem as Data.Layers.Entities.Utilisateur;
            new EditUtilisateurWindow(user).ShowDialog();
            Refresh();
        }

        private void Btn_DeleteUser(object sender, RoutedEventArgs e)
        {
            var messageBoxResult = DXMessageBox.Show("Etes-vous sûr de supprimer cet utilisateur ?", "Information", MessageBoxButton.YesNo, MessageBoxImage.Information);
            if (messageBoxResult == MessageBoxResult.No)
                return;
            try
            {
                if (SharedBll.Db.Effets.Any(ee => ee.Utilisateur.Id == SharedBll.CurrentUser.Id) || SharedBll.Db.Flexies.Any(ee => ee.Utilisateur.Id == SharedBll.CurrentUser.Id))
                {
                    DXMessageBox.Show("Vous ne pouvez pas supprimer cet utilisateur car il a effectué des activités", "information", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
                UtilisateurBll.RemoveUser(ManagGridControl.SelectedItem as Data.Layers.Entities.Utilisateur);

                SharedBll.Db.SaveChanges();
                DXMessageBox.Show("L'utilisateur a été supprimé avec succès", "information", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            Refresh();
        }
    }
}
