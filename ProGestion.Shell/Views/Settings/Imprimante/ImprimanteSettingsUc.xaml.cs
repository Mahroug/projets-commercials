﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Drawing.Printing;
using System.Collections.ObjectModel;
using DevExpress.Xpf.Core;
using ProGestion.Data.Bll;
using ProGestion.Resources;

namespace ProGestion.Shell.Views.Settings.Imprimante
{
    /// <summary>
    /// Interaction logic for ImprimanteSettingsUc.xaml
    /// </summary>
    public partial class ImprimanteSettingsUc : UserControl
    {
        public ImprimanteSettingsUc()
        {
            InitializeComponent();
            FillData();
        }

        public void FillData()
        {
            //Fill combobox
            try
            {
                var printerList = PrinterSettings.InstalledPrinters.Cast<string>().ToList();
                DefaultPrintCombobox.ItemsSource = printerList;
                DefaultPrintCombobox.SelectedItem = SharedBll.DefaultPrinter;
            }
            catch (Exception e)
            {
                DXMessageBox.Show(e.Message, "Erreur", MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }

        private void GeneralSettingsUC_OnUnloaded(object sender, RoutedEventArgs e)
        {
            SettingsWindow.DefaultPrinterCombobox = DefaultPrintCombobox.Text;
        }

        private void Save_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {

                SharedBll.DefaultPrinter = DefaultPrintCombobox.Text;
                /*
                * Settings.ini Start
                */
                var iniFile = new IniFile(SharedBll.AppPath + "settings.ini");
                iniFile.IniWriteValue("Settings", "DefaultPrinter ", SharedBll.DefaultPrinter);

                DXMessageBox.Show("Imprimante par défault a été enregistrée avec succès", "info", MessageBoxButton.OK,
                    MessageBoxImage.Information);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);

            }
        }
    }
}
