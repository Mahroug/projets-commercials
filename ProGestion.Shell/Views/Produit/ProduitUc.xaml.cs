﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using DevExpress.Data;
using System.Drawing;
using System.Drawing.Printing;
using DevExpress.Utils.Extensions;
using DevExpress.Xpf.Core;
using ProGestion.Data.Bll;
using ProGestion.Shell.Views.Effet.Livraison;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using UserControl = System.Windows.Controls.UserControl;
using ZXing;
using ZXing.Rendering;

namespace ProGestion.Shell.Views.Produit
{
    public partial class ProduitUc : UserControl
    {
        System.Drawing.Bitmap image;
        private ObservableCollection<Data.Layers.Entities.Produit> _manageSource =
            new ObservableCollection<Data.Layers.Entities.Produit>();

        public ProduitUc()
        {
            InitializeComponent();
            Refresh();
            ManagTableView.BestFitColumns();
            ManagGridControl.TotalSummary.Add(SummaryItemType.Count, "Id");
        }

       private void Refresh()
        {
            //change font size
            ManagGridControl.FontSize = SharedBll.UcFontSize;

            _manageSource = null;
            _manageSource = new ObservableCollection<Data.Layers.Entities.Produit>(SharedBll.Db.Produits.Local);

            if (!_manageSource.Any()) return;
            try
            {
                ManagGridControl.ItemsSource = null;
                ManagTableView.RowMinHeight = SharedBll.UcFontSize * 2.85;
                Search(true);
            }
            catch (Exception e)
            {
                DXMessageBox.Show(e.Message, "error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddproduitClick(object sender, RoutedEventArgs e)
        {
            new AddProduitWindow().ShowDialog();
            Refresh();
        }

        private void SearchBox_OnKeyUp(object sender, KeyEventArgs e)
        {
            Search(SearchBox.Text.Trim() != String.Empty);
        }

        private void Search(bool keyUpEmptyText)
        {
            try
            {
                ManagGridControl.ItemsSource = null;

                if (SearchBox.Text.Trim() == String.Empty && keyUpEmptyText)
                {
                    ManagGridControl.ItemsSource =
                        new ObservableCollection<Data.Layers.Entities.Produit>(SharedBll.Db.Produits.Local);
                    return;
                }

                ManagGridControl.ItemsSource = null;
                ManagGridControl.ItemsSource = _manageSource.Where(c =>
                    (c.Designation != null && c.Designation.ToLower().Contains(SearchBox.Text.ToLower())) ||
                    (c.Reference != null && c.Reference.ToString().Contains(SearchBox.Text.ToLower())) ||
                    (c.Description != null && c.Description.ToString().Contains(SearchBox.Text.ToLower())) ||
                    (c.Code != null && c.Code.ToString().Contains(SearchBox.Text.ToLower())) ||
                    (c.Lots.FirstOrDefault().CodeBar != null && c.Lots.FirstOrDefault().CodeBar.ToString().Contains(SearchBox.Text.ToLower()))
                );
            }
            catch (Exception exception)
            {
            }

        }

        private void EditProduitClick(object sender, RoutedEventArgs e)
        {
            var produit = ManagGridControl.SelectedItem as Data.Layers.Entities.Produit;
            if (produit == null) return;

            new EditProduitWindow(produit).ShowDialog();
            Refresh();
        }

        private void DeleteProduitClick(object sender, RoutedEventArgs e)
        {
            if (ManagGridControl.SelectedItem == null) return;

            Data.Layers.Entities.Produit produit;
            try
            {
                produit = ManagGridControl.SelectedItem as Data.Layers.Entities.Produit;
                var result = DXMessageBox.Show("Voulez vous supprimer cette ligne?", "Info", MessageBoxButton.YesNo,
                    MessageBoxImage.Warning);
                if (result != MessageBoxResult.Yes) return;

                ProduitBll.RemoveProduct(produit);
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            SharedBll.Db.SaveChanges();
            DXMessageBox.Show("suppression avec succès", "info", MessageBoxButton.OK, MessageBoxImage.Information);
            Search(true);
        }

        private void Excel_OnClick(object sender, RoutedEventArgs e)
        {

            try
            {
                var saveFileDialog = new SaveFileDialog
                {
                    InitialDirectory = Convert.ToString(Environment.SpecialFolder.Desktop),
                    Filter = @"(*.XLSX)|*.xlsx|All Files (*.*)|*.*",
                    FilterIndex = 1,
                    FileName = "List"
                };

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                    ManagTableView.ExportToXlsx(saveFileDialog.FileName);
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);

            }

        }

        private void MenuItemCodeBar_OnClick(object sender, RoutedEventArgs e)
        {
            var produit = ManagGridControl.SelectedItem as Data.Layers.Entities.Produit;
            if (produit == null)return;
            new PrintCodeBar(produit).ShowDialog();
            //Print(produit);
        }

        private void Print(Data.Layers.Entities.Produit produit)
        {
            try
            {
                    var writer = new BarcodeWriter
                    {
                        Format = BarcodeFormat.CODE_128,
                        Options = new ZXing.Common.EncodingOptions
                        {
                            Height = 120,
                            Width = 400,
                            Margin = 1,

                        },
                        Renderer = new BitmapRenderer()
                        {
                            TextFont = new Font(new System.Drawing.FontFamily("Segoe UI"), 14),
                        }
                    };

                var lot = produit.Lots.FirstOrDefault();
                if (lot == null) return;
                if (lot.CodeBar == "" || lot.CodeBar == null)
                {
                    var co = lot.Id + "2019";
                    switch (co.Length)
                    {
                        case 1:
                            co = "0000000" + co;
                            break;
                        case 2:
                            co = "000000" + co;
                            break;
                        case 3:
                            co = "00000" + co;
                            break;
                        case 4:
                            co = "0000" + co;
                            break;
                        case 5:
                            co = "000" + co;
                            break;
                        case 6:
                            co = "00" + co;
                            break;
                    }
                    lot.CodeBar = co;
                }
              
                
                    image = writer.Write(lot.CodeBar);
                ///////
                var printDialog = new System.Windows.Forms.PrintDialog();
                var printDocument = new PrintDocument();
                printDialog.Document = printDocument;
                printDocument.DefaultPageSettings.Margins = new Margins(5, 5, 5, 5);
                printDocument.PrintPage += PrintDocument_PrintPage;
                printDialog.PrinterSettings.PrinterName = SharedBll.DefaultPrinter;
                printDocument.Print();
            }
            catch (Exception exception)
            {
                DevExpress.Xpf.Core.DXMessageBox.Show(exception.Message, "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);

            }

        }

        private void PrintDocument_PrintPage(object sender, PrintPageEventArgs e)
        {
            var graphic = e.Graphics;

                //graphic.DrawImage(CarteFidilite.BackSide, new PointF(0, 0));
                //System.Drawing.Image image2 = System.Drawing.Image.FromFile(fileName);
                graphic.DrawImage(image, new System.Drawing.Rectangle(0, 0, (int)120, (int)60));

        }

        private void SaleProduitClick(object sender, RoutedEventArgs e)
        {
            if (!(ManagGridControl.SelectedItem is Data.Layers.Entities.Produit produit)) return;
            var parentWindow = Window.GetWindow(this) as MainWindow;
            parentWindow?.MainNavigationFrame.Navigate(new AddLivraisonUc(produit.Lots.FirstOrDefault()));
            
        }
    }
}
