﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;
using DevExpress.Xpf.Core;
using ProGestion.Data.Bll;

namespace ProGestion.Shell.Views.Produit
{
    /// <summary>
    /// Logique d'interaction pour EditProduitWindow.xaml
    /// </summary>
    public partial class EditProduitWindow
    {
        public Data.Layers.Entities.Produit Produit = new Data.Layers.Entities.Produit();
        public EditProduitWindow(Data.Layers.Entities.Produit produit)
        {
            InitializeComponent();
            this.Produit = produit;
            FillData();
        }

        private void EditProduitWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            Categorie.ItemsSource = SharedBll.Db.CategorieProduits.Local.ToArray();
            Marque.ItemsSource = SharedBll.Db.Marques.Local.ToArray();
        }

        private void FillData()
        {
            Designation.Text = Produit.Designation.ToString();
            Marque.SelectedItem = Produit.Marque as Data.Layers.Entities.Marque;
            Categorie.SelectedItem = Produit.CategorieProduit as Data.Layers.Entities.CategorieProduit;
            Reference.Text = Produit.Reference;
            stockMinimum.Text = Produit.stockMinimum.ToString();
            PrixAchat.Text = Produit.Lots.FirstOrDefault()?.PrixAchat.ToString();
            PrixVente.Text = Produit.Lots.FirstOrDefault()?.PrixVente.ToString();
        }

        private void SaveClick(object sender, RoutedEventArgs e)
        {
            if (!VerifyFieldsFields()) return;
            EditProduit.IsEnabled = false;
            try
            {
                Produit.Designation = Designation.Text;
                //Description = Description.,
                Produit.Marque = Marque?.SelectedItem as Data.Layers.Entities.Marque;
                Produit.CategorieProduit = Categorie?.SelectedItem as Data.Layers.Entities.CategorieProduit;
                Produit.Reference = Reference.Text;
                Produit.stockMinimum = Convert.ToInt32(stockMinimum?.Text);
                if(Produit.Lots.Any())
                {
                    Produit.Lots.FirstOrDefault().PrixAchat = Convert.ToDouble(PrixAchat?.Text);
                    Produit.Lots.FirstOrDefault().PrixVente = Convert.ToDouble(PrixVente?.Text);
                }

                ProduitBll.UpdateProduct(Produit);
                SharedBll.Db.SaveChanges();

                EditProduit.IsEnabled = true;
                Close();
                DXMessageBox.Show("La modification a été éffectuée avec succès", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private bool VerifyFieldsFields()
        {

            if (Designation.Text == "")
            {
                Designation.Focus();
                return false;
            }

            return true;
        }

        private void Montant_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex _regex = new Regex("[^0-9]+");
            e.Handled = _regex.IsMatch(e.Text);
        }
    }
}
