﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;
using DevExpress.Xpf.Core;
using ProGestion.Data.Bll;
using ProGestion.Shell.Views.CategorieProduit;

namespace ProGestion.Shell.Views.Produit
{
    /// <summary>
    /// Logique d'interaction pour AddProduitWindow.xaml
    /// </summary>
    public partial class AddProduitWindow
    {
        public AddProduitWindow()
        {
            InitializeComponent();
        }

        private bool VerifyFieldsFields()
        {
            if (Designation.Text == "")
            {
                Designation.Focus();
                return false;
            }

            return true;
        }

        private void Montant_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex _regex = new Regex("[^0-9]+");
            e.Handled = _regex.IsMatch(e.Text);
        }

        private void SaveClick(object sender, RoutedEventArgs e)
        {
            if (!VerifyFieldsFields()) return;
            try
            {
                var produit = new Data.Layers.Entities.Produit()
                {
                    Designation = Designation.Text,
                    Marque = Marque?.SelectedItem as Data.Layers.Entities.Marque,
                    CategorieProduit = Categorie?.SelectedItem as Data.Layers.Entities.CategorieProduit,
                    Reference = Reference?.Text,
                    stockMinimum = stockMinimum.Text != "" ? Convert.ToInt32(stockMinimum?.Text) : 0
                };
                var b=ProduitBll.AddProduct(produit);
                if (b)
                {
                    DXMessageBox.Show("Ce Produit existe déje", "info", MessageBoxButton.OK,
                        MessageBoxImage.Information);
                    Close();
                    return;
                }
                SharedBll.Db.SaveChanges();
                var lot = new Data.Layers.Entities.Lot()
                {
                    Produit = produit,
                    PrixAchat = PrixAchat.Text != "" ? Convert.ToInt32(PrixAchat?.Text) : 0,
                    PrixVente = PrixVente.Text != "" ? Convert.ToInt32(PrixVente?.Text) : 0
                };
                LotBll.AddLot(lot);
                SharedBll.Db.SaveChanges();
                DXMessageBox.Show("L'ajout a été fait avec succès", "info", MessageBoxButton.OK, MessageBoxImage.Information);
                Close();
                
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            Marque.ItemsSource = SharedBll.Db.Marques.Local.ToArray();
            Categorie.ItemsSource = SharedBll.Db.CategorieProduits.Local.ToArray();
        }

    }
}
