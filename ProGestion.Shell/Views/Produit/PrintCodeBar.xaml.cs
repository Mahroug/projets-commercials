﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using DevExpress.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Text.RegularExpressions;
using System.Windows.Input;
using DevExpress.Utils.Extensions;
using DevExpress.Xpf.Core;
using ProGestion.Data.Bll;
using ProGestion.Shell.Views.Effet.Livraison;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using UserControl = System.Windows.Controls.UserControl;
using ZXing;
using ZXing.Rendering;

namespace ProGestion.Shell.Views.Produit
{
    /// <summary>
    /// Logique d'interaction pour PrintCodeBar.xaml
    /// </summary>
    /// 
    public partial class PrintCodeBar
    {
        System.Drawing.Bitmap image;
        private Data.Layers.Entities.Produit produit;
        public PrintCodeBar()
        {
            InitializeComponent();
        }

        private void Montant_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex _regex = new Regex("[^0-9]+");
            e.Handled = _regex.IsMatch(e.Text);
        }

        public PrintCodeBar(Data.Layers.Entities.Produit produit)
        {
            this.produit = produit;
            InitializeComponent();
        }

        private void SaveClick(object sender, RoutedEventArgs e)
        {
            int ii = nombre.Value != 0 ? Convert.ToInt32(nombre.Text) : 0;
            for (int i = 0; i < ii; i++)
            {
                Print(produit);
            }
        }

        private void Print(Data.Layers.Entities.Produit produit)
        {
            try
            {
                var writer = new BarcodeWriter
                {
                    Format = BarcodeFormat.CODE_128,
                    Options = new ZXing.Common.EncodingOptions
                    {
                        Height = 40,
                        Width = 120,
                        Margin = 0,

                    },
                    Renderer = new BitmapRenderer()
                    {
                        TextFont = new Font(new System.Drawing.FontFamily("Segoe UI"), 7),
                    }
                };

                var lot = produit.Lots.FirstOrDefault();
                if (lot == null) return;
                if (string.IsNullOrEmpty(lot.CodeBar))
                {
                    var co = lot.Id + "2019";
                    switch (co.Length)
                    {
                        case 1:
                            co = "0000000" + co;
                            break;
                        case 2:
                            co = "000000" + co;
                            break;
                        case 3:
                            co = "00000" + co;
                            break;
                        case 4:
                            co = "0000" + co;
                            break;
                        case 5:
                            co = "000" + co;
                            break;
                        case 6:
                            co = "00" + co;
                            break;
                    }
                    lot.CodeBar = co;
                }


                image = writer.Write(lot.CodeBar);
                ///////
                var printDialog = new System.Windows.Forms.PrintDialog();
                var printDocument = new PrintDocument();
                printDialog.Document = printDocument;
                printDocument.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);
                printDocument.PrintPage += PrintDocument_PrintPage;
                printDialog.PrinterSettings.PrinterName = SharedBll.DefaultPrinter;
                printDocument.Print();
            }
            catch (Exception exception)
            {
                DevExpress.Xpf.Core.DXMessageBox.Show(exception.Message, "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);

            }

        }

        private void PrintDocument_PrintPage(object sender, PrintPageEventArgs e)
        {
            var graphic = e.Graphics;

            //graphic.DrawImage(CarteFidilite.BackSide, new PointF(0, 0));
            //System.Drawing.Image image2 = System.Drawing.Image.FromFile(fileName);
            graphic.DrawString("MALEK TELECOM   " + produit.Lots.FirstOrDefault().PrixVente + ".00 DA", new Font(new System.Drawing.FontFamily("Segoe UI"), 8), new SolidBrush(System.Drawing.Color.Black), 5, 0);
            //graphic.DrawImage(image, new System.Drawing.Rectangle(0, 15, (int)120, (int)50));
            graphic.DrawImage(image, new PointF(0, 14));


        }
    }
}
