﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Xpf.Core;
using ProGestion.Data.Bll;
using ProGestion.Data.Layers.Entities;
using ProGestion.Shell.Views.CategoriePersonne;


namespace ProGestion.Shell.Views.Fournisseur
{
    /// <summary>
    /// Logique d'interaction pour EditFournisseurWindow.xaml
    /// </summary>
    public partial class EditFournisseurWindow
    {
        public Data.Layers.Entities.Personne Fournisseur = new Data.Layers.Entities.Personne();
        public EditFournisseurWindow(Data.Layers.Entities.Personne fournisseur)
        {
            InitializeComponent();
            this.Fournisseur = fournisseur;
            FillData();
        }
        private void EditFournisseurWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            SexeComboBox.ItemsSource = new string[2] { "Masculin", "Féminin" };
            CategoriePersonne.ItemsSource = SharedBll.Db.CategoriePersonnes?.ToArray();
        }

        private void FillData()
        {
            if (Fournisseur.Sexe == 'm')
                SexeComboBox.SelectedItem = "Masculin";
            else if (Fournisseur.Sexe == 'f')
                SexeComboBox.SelectedItem = "Féminin";

            CategoriePersonne.SelectedItem = Fournisseur?.CategoriePersonne;
            Nom.Text = Fournisseur.Nom;
            Prenom.Text = Fournisseur.Prenom;
            Addresse.Text = Fournisseur.Adresse;
            Email.Text = Fournisseur.Email;
            Mobile.Text = Fournisseur.Mobile;
            Telephone.Text = Fournisseur.Telephone;
        }

        private void SaveClick(object sender, RoutedEventArgs e)
        {
            if (!VerifyFieldsFields()) return;
            EditFournisseur.IsEnabled = false;
            try
            {
                var sexe = SexeComboBox?.SelectedItem as string;
                var sexeDesignation = new Char();
                if (sexe == "Masculin")
                    sexeDesignation = 'm';
                else if (sexe == "Féminin")
                    sexeDesignation = 'f';

                Fournisseur.CategoriePersonne = CategoriePersonne.SelectedItem as Data.Layers.Entities.CategoriePersonne;
                Fournisseur.Nom = Nom.Text;
                Fournisseur.Prenom = Prenom.Text;
                Fournisseur.Sexe = sexe != null ? sexeDesignation : new Char();
                Fournisseur.Adresse = Addresse.Text;
                Fournisseur.Email = Email.Text;
                Fournisseur.Mobile = Mobile.Text;
                Fournisseur.Telephone = Telephone.Text;
                Fournisseur.TypePersonne = SharedBll.Db.TypePersonnes.FirstOrDefault(p => p.Id == 2);
                Fournisseur.DateCreation = DateTime.Now;
                Fournisseur.CreePar = SharedBll.CurrentUser;

                PersonneBll.UpdatePersonne(Fournisseur);
                SharedBll.Db.SaveChanges();

                EditFournisseur.IsEnabled = true;
                Close();
                DXMessageBox.Show("La modification a été fait avec succès", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Phone_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex _regex = new Regex("[^0-9]+");
            e.Handled = _regex.IsMatch(e.Text);
        }

        private void AddCategorieClick(object sender, RoutedEventArgs e)
        {
            var newCategorie = new AddCategoriePersonneWindow();
            newCategorie.ShowDialog();
            EditFournisseurWindow_OnLoaded(null, null);
        }

        private bool VerifyFieldsFields()
        {

            if (Nom.Text == "")
            {
                Nom.Focus();
                return false;
            }

            if (Prenom.Text == "")
            {
                Prenom.Focus();
                return false;
            }

            return true;
        }
    }
}
