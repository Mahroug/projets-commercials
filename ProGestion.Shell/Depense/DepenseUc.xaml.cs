﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DevExpress.Xpf.Core;
using ProGestion.Data.Bll;
using Exception = System.Exception;

namespace ProGestion.Shell.Views.Depense
{
    /// <summary>
    /// Logique d'interaction pour DepenseUc.xaml
    /// </summary>
    public partial class DepenseUc : UserControl
    {
        private ObservableCollection<Data.Layers.Entities.Effet> _manageSource = new ObservableCollection<Data.Layers.Entities.Effet>();
        public DepenseUc()
        {
            InitializeComponent();
            Refresh();
        }

        private void AddDepenseClick(object sender, RoutedEventArgs e)
        {
            new AddDepenseWindow().ShowDialog();
            Refresh();
        }

        private void EditDepenseClick(object sender, RoutedEventArgs e)
        {
            new EditDepenseWindow(ManagGridControl.SelectedItem as Data.Layers.Entities.Effet).ShowDialog();
            Refresh();
        }

        private void DeleteDepenseClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var x = DXMessageBox.Show("voulez-vous supprimé cette dépense?", "info", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (x == MessageBoxResult.Yes)
                {
                   EffetBll.RemoveEffet(ManagGridControl.SelectedItem as Data.Layers.Entities.Effet);
                 DXMessageBox.Show("Vous ne pouvez pas supprimer cette dépense", "info", MessageBoxButton.OK, MessageBoxImage.Information);
                    SharedBll.Db.SaveChanges();
                    Refresh();
                }
            }

            catch (Exception ex)
            {
                DXMessageBox.Show(ex.Message, "error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void SearchBox_OnKeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                ManagGridControl.ItemsSource = null;


                ManagGridControl.ItemsSource = SharedBll.Db.Effets.Local.Where(c => (c.TypeEffet?.Abreviation == "DEP") && (c.Note.ToLower().Contains(SearchBox.Text.ToLower())));
            }
            catch (Exception exception)
            {

            }
        }

        private void MenuItemCopy_OnClick(object sender, RoutedEventArgs e)
        {
            object cellValue = null;
            if (ManagTableView?.FocusedColumn != null)
            {
                cellValue = ManagGridControl.GetFocusedRowCellValue(ManagTableView?.FocusedColumn);
            }
            if (cellValue != null)
                Clipboard.SetText(cellValue.ToString());
        }

        private void Refresh()
        {
            try
            {
                _manageSource = null;
                _manageSource = new ObservableCollection<Data.Layers.Entities.Effet>(SharedBll.Db.Effets.Local.Where(c => c.TypeEffet?.Abreviation == "DEP"));

                ManagGridControl.ItemsSource = _manageSource;
            }

            catch (Exception ex)
            {
                DXMessageBox.Show(ex.Message, "error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
