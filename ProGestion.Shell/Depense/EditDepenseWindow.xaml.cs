﻿using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;
using DevExpress.Xpf.Core;
using ProGestion.Data.Bll;
using ProGestion.Data.Layers.Entities;

namespace ProGestion.Shell.Views.Depense
{
    /// <summary>
    /// Logique d'interaction pour EditDepenseWindow.xaml
    /// </summary>
    public partial class EditDepenseWindow
    {
        public Data.Layers.Entities.Effet Depense = new Data.Layers.Entities.Effet();
        public EditDepenseWindow(Data.Layers.Entities.Effet depense)
        {
            InitializeComponent();
            this.Depense = depense;
            FillData();
        }

        private void Montant_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex _regex = new Regex("[^0-9]+");
            e.Handled = _regex.IsMatch(e.Text);
        }

        private void FillData()
        {
            MontantTTC.Text = Depense.MontantTTC.ToString(CultureInfo.InvariantCulture);
            Note.Text = Depense.Note.ToString(CultureInfo.InvariantCulture);
        }

        private void SaveClick(object sender, RoutedEventArgs e)
        {
            if (!VerifyFieldsFields()) return;
            EditDepense.IsEnabled = false;
            try
            {
                Depense.MontantTTC = Convert.ToDouble(MontantTTC.Text);
                Depense.Note = Note.Text;
                Depense.DerniereModification = DateTime.Now;
                Depense.DernierUtilisateur = SharedBll.CurrentUser;

                EffetBll.UpdateEffet(Depense);
                SharedBll.Db.SaveChanges();

                EditDepense.IsEnabled = true;
                Close();
                DXMessageBox.Show("Dépense Modifié avec succès", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private bool VerifyFieldsFields()
        {
            if (MontantTTC.Text == "")
            {
                MontantTTC.Focus();
                return false;
            }

            if (Note.Text == "")
            {
                Note.Focus();
                return false;
            }

            return true;
        }
    }
}
