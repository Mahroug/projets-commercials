﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;
using DevExpress.Xpf.Core;
using ProGestion.Data.Bll;
using ProGestion.Data.Layers.Entities;

namespace ProGestion.Shell.Views.Depense
{
    public partial class AddDepenseWindow
    {
        public AddDepenseWindow()
        {
            InitializeComponent();
        }

        private void Montant_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex _regex = new Regex("[^0-9]+");
            e.Handled = _regex.IsMatch(e.Text);
        }

        private void SaveClick(object sender, RoutedEventArgs e)
        {
            ///// verify fields
            if (!VerifyFieldsFields()) return;
            AddDepense.IsEnabled = false;
            try
            {
                var depense = new Data.Layers.Entities.Effet
                {
                    DetailsEffets = new ObservableCollection<DetailsEffet>(),
                    Utilisateur = SharedBll.CurrentUser,
                    DateCreation = DateTime.Now,
                    TypeEffet = TypeEffetBll.GetEffet("DEP"),
                    MontantTTC = Convert.ToDouble(MontantTTC.Text),
                    Note = Note.Text,
                    Reste = 0 // add window for credit
                };
                EffetBll.AddEffet(depense);
                SharedBll.Db.SaveChanges();

                AddDepense.IsEnabled = true;
                Close();
                DXMessageBox.Show("Dépense ajoutée avec succès", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }
        private bool VerifyFieldsFields()
        {
            if (MontantTTC.Text == "")
            {
                MontantTTC.Focus();
                return false;
            }

            if (Note.Text == "")
            {
                Note.Focus();
                return false;
            }

            return true;
        }
    }
}
