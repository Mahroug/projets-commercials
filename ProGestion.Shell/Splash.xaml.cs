﻿using ProGestion.Data.Bll;
using ProGestion.Data.Layers;
using ProGestion.Data.Layers.Entities;
using ProGestion.Resources;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Management;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using DevExpress.Xpf.Core;
using Application = System.Windows.Application;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using MessageBox = System.Windows.MessageBox;


namespace ProGestion.Shell
{
    /// <summary>
    /// Logique d'interaction pour Splash.xaml
    /// </summary>
    public partial class Splash
    {
        public static bool IsFirstLoad = true;
        public List<Settings> Settings = new List<Settings>();
        public Splash()
        {
            InitializeComponent();
        }
        public Splash(bool isFirstLoad)
        {
            InitializeComponent();
            IsFirstLoad = isFirstLoad;
        }

        private async void Splash_OnLoaded(object sender, RoutedEventArgs e)
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("fr");
            try
            {
                //ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive");

                //string serial_number = "";
                //foreach (ManagementObject wmi_HD in searcher.Get())
                //{
                //    serial_number = wmi_HD["SerialNumber"].ToString();
                //}
                //if (serial_number != "50026B7682A91A89")
                //{
                //    DXMessageBox.Show("Vous n'avez pas une clé d'activation . Veuillez nous contacter SVP", "Info", MessageBoxButton.OK,
                //        MessageBoxImage.Information);
                //    Button_Exit(null, null);
                //}
                //string hostName = System.Net.Dns.GetHostName();
                //if (hostName != "DESKTOP-R4SQRP7")
                //{
                //    DXMessageBox.Show("Vous n'avez pas une clé d'activation . Veuillez nous contacter SVP", "Info", MessageBoxButton.OK,
                //        MessageBoxImage.Information);
                //    Button_Exit(null, null);
                //}
                //var macAddr =
                //(
                //    from nic in NetworkInterface.GetAllNetworkInterfaces()
                //    where nic.OperationalStatus == OperationalStatus.Up
                //    select nic.GetPhysicalAddress().ToString()
                //).FirstOrDefault();

            }
            catch (Exception ee)
            {
                Console.WriteLine(ee.Message);
            }


            if (IsFirstLoad)
            {
                var path = AppDomain.CurrentDomain.BaseDirectory;

                var iniFile = new IniFile(path + "settings.ini");

                SharedBll.ConnectionString = iniFile.IniReadValue("Connection", "local");
                try
                {
                    SharedBll.Db = new ProGestionDbContext(SharedBll.ConnectionString);
                }

                catch (Exception sqlException)
                {
                    MessageBox.Show("Database error " + sqlException.Message);
                    Application.Current.Shutdown();
                    return;
                }

             // ini file

                //DefaultTicketPrinter
                var defaultTicketPrinter = iniFile.IniReadValue("Settings", "DefaultTicketPrinter").Trim();
                if (defaultTicketPrinter == string.Empty)
                {
                    iniFile.IniWriteValue("Settings", "DefaultTicketPrinter", "");
                    SharedBll.DefaultTicketPrinter = "";
                }
                else
                    SharedBll.DefaultTicketPrinter = defaultTicketPrinter;

                //DefaultPrinter
                var defaultPrinter = iniFile.IniReadValue("Settings", "DefaultPrinter").Trim();
                if (defaultPrinter == string.Empty)
                {
                    iniFile.IniWriteValue("Settings", "DefaultPrinter", "");
                    SharedBll.DefaultPrinter = "";
                }
                else
                    SharedBll.DefaultPrinter = defaultPrinter;

                //UcFontSize
                var ucFontSize = iniFile.IniReadValue("Settings", "UcFontSize").Trim();
                if (ucFontSize == string.Empty)
                {
                    iniFile.IniWriteValue("Settings", "UcFontSize", "14");
                    SharedBll.UcFontSize = 14;
                }
                else
                    SharedBll.UcFontSize = Convert.ToInt32(ucFontSize);

                //Version
                var localVersion = iniFile.IniReadValue("Settings", "Version").Trim();
                if (localVersion == string.Empty)
                {
                    iniFile.IniWriteValue("Settings", "Version", "");
                    SharedBll.LocalVersion = "";
                }
                else
                    SharedBll.LocalVersion = localVersion;



                ////lis
                //var lis = iniFile.IniReadValue("Settings", "license").Trim();
                //if (lis == string.Empty)
                //{
                //    iniFile.IniWriteValue("Settings", "license", Tools.CryptedPassword("BMKA1"));
                //}
                //iniFile.IniWriteValue("Settings", "IsUsed", "true");
                


                LoadingText("Catégories");
                await SharedBll.Db.CategoriePersonnes.LoadAsync();
                await SharedBll.Db.CategorieProduits.LoadAsync();


                LoadingText("Fournisseur");
                await SharedBll.Db.DetailsEffets.LoadAsync();
                await SharedBll.Db.Effets.LoadAsync();
                await SharedBll.Db.EtatEffets.LoadAsync();


                LoadingText("Produits");
                await SharedBll.Db.Lots.LoadAsync();

                LoadingText("Marques");
                await SharedBll.Db.Marques.LoadAsync();

                LoadingText("Client");
                await SharedBll.Db.Settings.LoadAsync();

                LoadingText("Settings");
                await SharedBll.Db.Personnes.LoadAsync();

                LoadingText("Roles");
                await SharedBll.Db.Produits.LoadAsync();
            

                LoadingText("Utlisateures");
                await SharedBll.Db.Roles.LoadAsync();
                await SharedBll.Db.Solvabilites.LoadAsync();

                LoadingText("Fournisseur");
                await SharedBll.Db.TypeEffets.LoadAsync();

                LoadingText("Client");
                await SharedBll.Db.TypePersonnes.LoadAsync();
                await SharedBll.Db.Utilisateurs.LoadAsync();

                LoadingText("Flexy");
                await SharedBll.Db.Flexies.LoadAsync();
                await SharedBll.Db.Operateurs.LoadAsync();



                //Check if working with local db load local paramaters




                Settings = SharedBll.Db.Settings.Local.ToList();
                // Admin user
                var admin = UtilisateurBll.GetAdmin();
                if (admin == null)
                {
                    SharedBll.Db.Utilisateurs.Local.Add(new Utilisateur()
                    {
                        Username = "admin",
                        Password = Tools.CryptedPassword("123"),
                        Role = new Role()
                        {
                            Designation="administrateur",                                                    
                        }
                    });
                }

                // client
                var client = TypePersonneBll.GetClient();
                if (client == null)
                {
                    SharedBll.Db.TypePersonnes.Local.Add(new TypePersonne()
                    {
                        Designation = "client"
                       
                    });
                }

                // fournisseur
                var fournisseur = TypePersonneBll.GetFournisseur();
                if (fournisseur == null)
                {
                    SharedBll.Db.TypePersonnes.Local.Add(new TypePersonne()
                    {
                        Designation = "fournisseur"

                    });
                }

                // BR
                var BR = TypeEffetBll.GetBR();
                if (BR == null)
                {
                    SharedBll.Db.TypeEffets.Local.Add(new TypeEffet()
                    {
                        Designation = "bon de reception",
                        Abreviation = "BR"

                    });
                }

                // BRF
                var BRF = TypeEffetBll.GetBRF();
                if (BRF == null)
                {
                    SharedBll.Db.TypeEffets.Local.Add(new TypeEffet()
                    {
                        Designation = "retour fournisseur",
                        Abreviation = "BRF"

                    });
                }

                // FP
                var FP = TypeEffetBll.GetFP();
                if (FP == null)
                {
                    SharedBll.Db.TypeEffets.Local.Add(new TypeEffet()
                    {
                        Designation = "facture proformat",
                        Abreviation = "FP"

                    });
                }

                // BL
                var BL = TypeEffetBll.GetBL();
                if (BL == null)
                {
                    SharedBll.Db.TypeEffets.Local.Add(new TypeEffet()
                    {
                        Designation = "bon de livraison",
                        Abreviation = "BL"

                    });
                }
                // BRC
                var BRC = TypeEffetBll.GetBRC();
                if (BRC == null)
                {
                    SharedBll.Db.TypeEffets.Local.Add(new TypeEffet()
                    {
                        Designation = "retour client",
                        Abreviation = "BRC"

                    });
                }

                // BC
                var BC = TypeEffetBll.GetBC();
                if (BC == null)
                {
                    SharedBll.Db.TypeEffets.Local.Add(new TypeEffet()
                    {
                        Designation = "Bon de commande",
                        Abreviation = "BC"
                    });
                }

                // Effet Brouillon
                var BrouillonLivraison = TypeEffetBll.GetEffet("BROL");
                if (BrouillonLivraison == null)
                {
                    SharedBll.Db.TypeEffets.Local.Add(new TypeEffet()
                    {
                        Designation = "Brouillon Livraison",
                        Abreviation = "BROL"
                    });
                }

                // Dépense
                var Depense = TypeEffetBll.GetEffet("DEP");
                if (Depense == null)
                {
                    SharedBll.Db.TypeEffets.Local.Add(new TypeEffet()
                    {
                        Designation = "Dépense",
                        Abreviation = "DEP"
                    });
                }

                // Mobilis
                var Mobilis = OperateurBll.GetMobilis();
                if (Mobilis == null)
                {
                    SharedBll.Db.Operateurs.Local.Add(new Operateur()
                    {
                        Designation = "Mobilis",
                        
                    });
                }

                // Ooredoo
                var Ooredoo = OperateurBll.GetOoredoo();
                if (Ooredoo == null)
                {
                    SharedBll.Db.Operateurs.Local.Add(new Operateur()
                    {
                        Designation = "Ooredoo",

                    });
                }

                // Djezzy
                var Djezzy = OperateurBll.GetDjezzy();
                if (Djezzy == null)
                {
                    SharedBll.Db.Operateurs.Local.Add(new Operateur()
                    {
                        Designation = "Djezzy",

                    });
                }

                // Role
                var Role = RoleBll.GetByName("vendeur");
                if (Role == null)
                {
                    SharedBll.Db.Roles.Local.Add(new Role()
                    {
                        Designation = "vendeur"
                    });
                }

                // Version
                //var version = Settings.FirstOrDefault(x => x.KeyName == "Version");
                //if (version == null)
                //{
                //    SharedBll.Db.Settings.Local.Add(new Settings
                //    {
                //        KeyName = "Version",
                //        KeyValue = SharedBll.LocalVersion = localVersion,
                //        User = UserBll.GetAdmin()
                //    });
                //    SharedBll.Version = "1.0.0";
                //}
                //else
                //    SharedBll.Version = version.KeyValue;

                //ConnectionWooCommerceBll.FixProductsImages();

                //Assert config file changements
                SharedBll.Db.SaveChanges();
                
                LocalVersionTextBlock.Text = "Version 1.0" + SharedBll.LocalVersion;
               
            }

        }
        private void Button_Exit(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        

        private async Task<int> CheckFields()
        {
            
            if (UsernameBox.Text == "")
            {
                UsernameBox.Focus();
                return 0;
            }
            if (PasswordBox.Password == "")
            {
                PasswordBox.Focus();
                return 0;
            }

            if (UtilisateurBll.CheckLogin(new Utilisateur { Username = UsernameBox.Text, Password = Tools.CryptedPassword(PasswordBox.Password) }))
            {
                SharedBll.CurrentUser = UtilisateurBll.GetByUsername(UsernameBox.Text);
                new MainWindow().Show();
                Hide();
            }
            else
            {
                PasswordErr.Visibility = Visibility.Visible;
            }
            return 1;
        }

        private void Button_Login(object sender, RoutedEventArgs e)
        {
            CheckFields();
        }

        private void box_KeyUp(object sender, KeyEventArgs e)
        {
            PasswordErr.Visibility = Visibility.Hidden;
            if (UsernameBox.Text != "" && PasswordBox.Password != "")
            {
                LoginButton.IsEnabled = true;
            }
            else LoginButton.IsEnabled = false;

            if (e.Key == Key.Return)
            {
                CheckFields();
            }
        }

        private void LoadingText(string entity)
        {
           // LoadingTex.Text = "Chargement " + entity + "..";
        }

    }
}
