﻿using ProGestion.Data.Bll;
using ProGestion.Data.Layers;
using ProGestion.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DevExpress.Xpf.Bars;
using ProGestion.Shell.Views;
using DevExpress.Xpf.Core;
using System.ComponentModel;
using ProGestion.Shell.Views.CategorieProduit;
using ProGestion.Shell.Views.Client;
using ProGestion.Shell.Views.Flexy;
using ProGestion.Shell.Views.Fournisseur;
using ProGestion.Shell.Views.Produit;
using ProGestion.Shell.Views.CategoriePersonne;
using ProGestion.Shell.Views.Effet.Livraison;
using ProGestion.Shell.Views.Effet.Reception;
using ProGestion.Shell.Views.Marque;
using ProGestion.Shell.Views.Statistique;
using ProGestion.Shell.Views.Settings;
using ProGestion.Shell.Views.Depense;

namespace ProGestion.Shell
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            Console.WriteLine("Succés");
            if (SharedBll.CurrentUser.Role.Designation == "vendeur")
            {
                flexy.IsVisible = false;
                divers.IsVisible = false;
                achat.IsVisible = false;
                stockRibbon.IsVisible = false;
                statistique.IsVisible = false;
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            var result = DXMessageBox.Show("Êtes-vous sûr de vouloir sortir?", "Info",
                MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result != MessageBoxResult.Yes)
            {
                e.Cancel = true;
                return;
            }

            try
            {
                SharedBll.Db.SaveChanges();
            }
            catch (Exception exception)
            {
                DXMessageBox.Show(exception.Message, "error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void MainWindow_OnClosed(object sender, EventArgs e)
        {
            try
            {
                SharedBll.Db.SaveChanges();
            }
            catch (Exception exception)
            {
                DevExpress.Xpf.Core.DXMessageBox.Show(exception.Message, "error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            Application.Current.Shutdown();
        }

        private void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            UserLabel.Content = SharedBll.CurrentUser.Username;
            var produitSeuil = 0;
            foreach(var _produit in SharedBll.Db.Produits ) {
                if(_produit.isSeuilStock)
                    produitSeuil++;
            }
            stockMinimum.Content = produitSeuil;
        }

        private void Logout_OnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            try
            {
                SharedBll.Db.SaveChanges();
            }
            catch (Exception exception)
            {
                DevExpress.Xpf.Core.DXMessageBox.Show(exception.Message, "error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            var sp = new Splash(false);
            sp.Show();
            Hide();
        }

        private void ManageReceptionItemClick(object sender, ItemClickEventArgs e)
        {
            MainNavigationFrame.Navigate(new ReceptionUc());
        }

        private void ManageFlexy_ItemClick(object sender, ItemClickEventArgs e)
        {
            MainNavigationFrame.Navigate(new FlexyUc());
        }

        private void ManageMarques_ItemClick(object sender, ItemClickEventArgs e)
        {
            MainNavigationFrame.Navigate(new MarqueUc());
        }

        private void ManageCategorieProduits_ItemClick(object sender, ItemClickEventArgs e)
        {
            MainNavigationFrame.Navigate(new CategorieProduitUc());
        }

        private void ManageClients_ItemClick(object sender, ItemClickEventArgs e)
        {
            MainNavigationFrame.Navigate(new ClientUc());
        }

        private void ManageFournisseurs_ItemClick(object sender, ItemClickEventArgs e)
        {
            MainNavigationFrame.Navigate(new FournisseurUc());
        }

        private void ManageCategoriePersonne_ItemClick(object sender, ItemClickEventArgs e)
        {
            MainNavigationFrame.Navigate(new CategoriePersonneUc());
        }

        private void ManageProduits_ItemClick(object sender, ItemClickEventArgs e)
        {
            MainNavigationFrame.Navigate(new ProduitUc());
        }

        private void ManageRetourFournisseurItemClick(object sender, ItemClickEventArgs e)
        {
            MainNavigationFrame.Navigate(new RetourFournisseurUc());
        }

        private void ManageCommande_ItemClick(object sender, ItemClickEventArgs e)
        {
            MainNavigationFrame.Navigate(new CommandeUc());
        }

        private void ManageRecette_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void ManageBénefice_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void ManageLivraison_ItemClick(object sender, ItemClickEventArgs e)
        {
            MainNavigationFrame.Navigate(new LivraisonUc());
        }

        private void ManageProformat_ItemClick(object sender, ItemClickEventArgs e)
        {
            MainNavigationFrame.Navigate(new ProformatUc());
        }

        private void ManageRetourClient_ItemClick(object sender, ItemClickEventArgs e)
        {
            MainNavigationFrame.Navigate(new RetourClientUc());
        }

        private void ManageJournalReceptionItemClick(object sender, ItemClickEventArgs e)
        {
            MainNavigationFrame.Navigate(new JournalReceptionUc());
        }

        private void ManageJournalLivraison_ItemClick(object sender, ItemClickEventArgs e)
        {
            MainNavigationFrame.Navigate(new JournalLivraisonUc());
        }

        private void SettingsOnClick(object sender, RoutedEventArgs e)
        {
            var settings = new SettingsWindow();//SettingsNewLayout();// Settings();
            settings.ShowDialog();
        }

        private void ActualiserOnClick(object sender, RoutedEventArgs e)
        {
            var produitSeuil = 0;
            foreach (var _produit in SharedBll.Db.Produits)
            {
                if (_produit.isSeuilStock)
                    produitSeuil++;
            }
            stockMinimum.Content = produitSeuil;
        }

        private void ManageStat_ItemClick(object sender, ItemClickEventArgs e)
        {
            MainNavigationFrame.Navigate(new StatistiqueUc());
        }

        private void ManageDepense_ItemClick(object sender, ItemClickEventArgs e)
        {
            MainNavigationFrame.Navigate(new DepenseUc());
        }

        private void MainWindow_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F5)
            {
                MainNavigationFrame.Navigate(new AddLivraisonUc());
            }
            if (e.Key == Key.F6)
            {
                MainNavigationFrame.Navigate(new AddReceptionUc());
            }
        }
    }
}

