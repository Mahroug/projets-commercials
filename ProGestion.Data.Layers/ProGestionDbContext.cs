﻿using ProGestion.Data.Layers.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProGestion.Data.Layers
{
    public class ProGestionDbContext : DbContext
    {
        public ProGestionDbContext(string connectionString) : base(connectionString)
        {
            //Database.SetInitializer(new DropCreateDatabaseIfModelChanges<DbContext>());
            Database.CreateIfNotExists();
        }

        public DbSet<CategoriePersonne> CategoriePersonnes { get; set; }

        public DbSet<CategorieProduit> CategorieProduits { get; set; }

        public DbSet<DetailsEffet> DetailsEffets { get; set; }

        public DbSet<Effet> Effets { get; set; }

        public DbSet<EtatEffet> EtatEffets { get; set; }

        public DbSet<Lot> Lots { get; set; }

        public DbSet<Marque> Marques { get; set; }

        public DbSet<Personne> Personnes { get; set; }

        public DbSet<Produit> Produits { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Solvabilite> Solvabilites { get; set; }

        public DbSet<TypeEffet> TypeEffets { get; set; }

        public DbSet<TypePersonne> TypePersonnes { get; set; }

        public DbSet<Settings> Settings { get; set; }

        public DbSet<Utilisateur> Utilisateurs { get; set; }

        public DbSet<Operateur> Operateurs { get; set; }

        public DbSet<Flexy> Flexies { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Configuration.ProxyCreationEnabled = true;

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }
    }
}
