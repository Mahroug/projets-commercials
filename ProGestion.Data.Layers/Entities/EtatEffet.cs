﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProGestion.Data.Layers.Entities
{
    public class EtatEffet
    {
        [Key]
        public int Id { get; set; }
        public string Designation { get; set; }
        public string Couleur { get; set; }
        public virtual ICollection<Effet> Effets { get; set; }

    }
}
