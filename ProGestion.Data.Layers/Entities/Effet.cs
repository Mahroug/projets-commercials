﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProGestion.Data.Layers.Entities
{
    public class Effet
    {
        [Key]
        public int Id { get; set; }

        public string Code { get; set; }

        public double MontantTTC { get; set; }

        public double PrixAchatTotal { get; set; }

        public double Reste { get; set; }

        public DateTime DateCreation { get; set; }

        public DateTime? DerniereModification { get; set; }

        public Utilisateur Utilisateur { get; set; }

        public Utilisateur DernierUtilisateur { get; set; }

        public Effet Parent { get; set; }

        public Personne Personne { get; set; }

        public EtatEffet EtatEffet { get; set; }

        public TypeEffet TypeEffet { get; set; }

        public virtual ICollection<DetailsEffet> DetailsEffets { get; set; }

        public string PersonneToShow => HasPersonne ? Personne.FullName : "aucune information";

        public bool HasPersonne => Personne != null;

        public bool IsReste => Reste > 0;

        public string Note { get; set; }
    }
}
