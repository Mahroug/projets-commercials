﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProGestion.Data.Layers.Entities
{
    public class Produit
    {
        [Key]
        public int Id { get; set; }

        public string Code { get; set; }

        public string Reference { get; set; }
        public string Designation { get; set; }

        public string Description { get; set; }

        public int stockMinimum { get; set; }

        public bool isSeuilStock => Quantite <= stockMinimum;

        public int Quantite => Lots != null ? Lots.Sum(l => l.Quantite) : 0;

        public CategorieProduit CategorieProduit { get; set; }

        public Marque Marque { get; set; }

        public virtual ICollection<Lot> Lots { get; set; }
        public string MarqueToShow => HasMarque ? Marque.Designation : "(aucune information)";
        public bool HasMarque => Marque != null;
        public string CategorieToShow => HasCategorie ? CategorieProduit.Designation : "(aucune information)";
        public bool HasCategorie => CategorieProduit != null;
    }
}
