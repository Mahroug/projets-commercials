﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProGestion.Data.Layers.Entities
{
    public class DetailsEffet
    {
        [Key]
        public int Id { get; set; }
        public string Code { get; set; }
        public double PrixUnitairePaye { get; set; }
        public int Quantite { get; set; }
        public double TotalPrix => Quantite * PrixUnitairePaye;
        public Lot Lot { get; set; }
        public Effet Effet { get; set; }

    }
}
