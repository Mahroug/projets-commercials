﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ProGestion.Data.Layers.Entities
{
    public class Personne
    {
        [Key]
        public int Id { get; set; }

        public string Code { get; set; }

        public string Nom { get; set; }

        public string Prenom { get; set; }

        public string Telephone { get; set; }

        public string Mobile { get; set; }

        public string Adresse { get; set; }

        public char? Sexe { get; set; }

        public string SexeText
        {
            get
            {
                if (Sexe == 'm')
                    return "Masculin";
                else if (Sexe == 'f')
                    return "Féminin";
                else return "";
            }
        }

        public string Email { get; set; }

        public DateTime? DateCreation { get; set; }

        public DateTime? DateModification { get; set; }

        public Utilisateur CreePar { get; set; }

        public Utilisateur ModifierPar { get; set; }

        public TypePersonne TypePersonne { get; set; }

        public CategoriePersonne CategoriePersonne { get; set; }

        public Solvabilite Solvabilite { get; set; }

        public virtual ICollection<Effet> Effets { get; set; }

        public virtual ICollection<Flexy> Flexies { get; set; }

        public string FullName =>Nom + " "+Prenom;

        public double DetteFlexyF => Flexies?.Sum(f => f.Versement) - Flexies?.Sum(f => f.Montant) ?? 0;

        public double DetteFlexyC => - DetteFlexyF;

        public bool IsFournisseur => TypePersonne?.Id == 2;

        public bool IsClient => TypePersonne?.Id == 1;
    }
}
