﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProGestion.Data.Layers.Entities
{
    public class Flexy
    {
        [Key]
        public int Id { get; set; }
        public double Montant { get; set; }
        public double Versement { get; set; }
        public DateTime DateCreation { get; set; }
        public DateTime? DerniereModification { get; set; }
        public Utilisateur Utilisateur { get; set; }
        public Utilisateur DernierUtilisateur { get; set; }
        public Operateur Operateur { get; set; }
        public Personne Personne { get; set; }
    }
}
