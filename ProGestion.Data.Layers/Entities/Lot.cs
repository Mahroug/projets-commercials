﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProGestion.Data.Layers.Entities
{
    public class Lot
    {
        [Key]
        public int Id { get; set; }
        public string CodeBar { get; set; }
        public double PrixAchat { get; set; }
        public double PrixVente { get; set; }
        public double Dernier { get; set; }
        public DateTime? DateExpiration { get; set; }
        public int Quantite { get; set; }
        public Produit Produit { get; set; }
        public virtual ICollection<DetailsEffet> DetailsEffets { get; set; }
    }
}
