﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProGestion.Data.Layers.Entities
{
    public class Settings
    {
        [Key]
        public int Id { get; set; }

        public string KeyName { get; set; }

        public string KeyValue { get; set; }

        public Utilisateur utilisateur { get; set; }
    }
}
