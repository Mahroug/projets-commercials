﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProGestion.Data.Layers.Entities;

namespace ProGestion.Data.Bll
{
    public class CategorieProduitBll : SharedBll
    {
        public static bool AddCategorieProduit(CategorieProduit categorieProduit)
        {
            var found = Found(categorieProduit);
            if (!found)
            {
                Db.CategorieProduits.Local.Add(categorieProduit);
            }
            return found;
        }

        private static bool Found(CategorieProduit categorieProduit)
        {
            return Db.CategorieProduits.Local.Any(p => p.Id == categorieProduit.Id);
        }
        public static bool RemoveCategoryProduit(CategorieProduit categorieProduit)
        {
            try
            {
                Db.CategorieProduits.Local.Remove(categorieProduit);
                return true;
            }
            catch (Exception e)
            {
                throw new Exception();
                return false;
            }
            
        }

        public static CategorieProduit GetById(int id)
        {
            return Db.CategorieProduits.Local.FirstOrDefault(c => c.Id == id);
        }

        public static void UpdateCategorieProduit(CategorieProduit categorieProduit)
        {
            var x = Db.CategorieProduits.Local.First(c => c.Id == categorieProduit.Id);
            x.Id = categorieProduit.Id;
        }
    }
}
