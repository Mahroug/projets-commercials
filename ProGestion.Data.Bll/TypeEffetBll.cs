﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProGestion.Data.Layers.Entities;

namespace ProGestion.Data.Bll
{
    public class TypeEffetBll: SharedBll

    {
        public static TypeEffet GetEffet(string s)
        {
            return Db.TypeEffets.Local.SingleOrDefault(us => us.Abreviation == s);
        }

        public static TypeEffet GetBR()
        {
            return Db.TypeEffets.Local.SingleOrDefault(us => us.Abreviation == "BR");
        }
        public static TypeEffet GetBRF()
        {
            return Db.TypeEffets.Local.SingleOrDefault(us => us.Abreviation == "BRF");
        }
        public static TypeEffet GetBC()
        {
            return Db.TypeEffets.Local.SingleOrDefault(us => us.Abreviation == "BC");
        }



        public static TypeEffet GetBL()
        {
            return Db.TypeEffets.Local.SingleOrDefault(us => us.Abreviation == "BL");
        }
        public static TypeEffet GetBRC()
        {
            return Db.TypeEffets.Local.SingleOrDefault(us => us.Abreviation == "BRC");
        }
        public static TypeEffet GetFP()
        {
            return Db.TypeEffets.Local.SingleOrDefault(us => us.Abreviation == "FP");
        }

    }
}
