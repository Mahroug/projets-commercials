﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProGestion.Data.Layers.Entities;

namespace ProGestion.Data.Bll
{
    public class InternalClass
    {
        public class Recette
        {
            public DateTime? Date { get; set; }
            public double MontantV { get; set; }
            public double MontantA { get; set; }
            public double Depense { get; set; }
            public double Benefice => MontantV - MontantA;
            public double MontantCaisse => MontantV - Depense;

        }
       
    }
}
