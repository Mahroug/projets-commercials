﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProGestion.Data.Layers.Entities;

namespace ProGestion.Data.Bll
{
    public class CategoriePersonneBll : SharedBll
    {
        public static bool AddCategoriePersonne(CategoriePersonne categoriePersonne)
        {
            var found = Found(categoriePersonne);
            if (!found)
            {
                Db.CategoriePersonnes.Local.Add(categoriePersonne);
            }
            return found;
        }

        private static bool Found(CategoriePersonne categoriePersonne)
        {
            return Db.CategoriePersonnes.Local.Any(p => p.Id == categoriePersonne.Id);
        }
        public static bool RemoveCategoryPersonne(CategoriePersonne categoriePersonne)
        {
            try
            {
                Db.CategoriePersonnes.Local.Remove(categoriePersonne);
                return true;
            }
            catch (Exception e)
            {
              throw new Exception();
                return false;
            }
        }

        public static CategoriePersonne GetById(int id)
        {
            return Db.CategoriePersonnes.Local.FirstOrDefault(c => c.Id == id);
        }

        public static void UpdateCategoriePersonne(CategoriePersonne categoriePersonne)
        {
            var x = Db.CategoriePersonnes.Local.First(c => c.Id == categoriePersonne.Id);
            x.Id = categoriePersonne.Id;
        }
    }
}
