﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProGestion.Data.Layers.Entities;

namespace ProGestion.Data.Bll
{
    public class OperateurBll : SharedBll
    {
        public static Operateur GetMobilis()
        {
            return Db.Operateurs.Local.SingleOrDefault(us => us.Designation == "Mobilis");
        }
        public static Operateur GetOoredoo()
        {
            return Db.Operateurs.Local.SingleOrDefault(us => us.Designation == "Ooredoo");
        }
        public static Operateur GetDjezzy()
        {
            return Db.Operateurs.Local.SingleOrDefault(us => us.Designation == "Djezzy");
        }
    }
}
