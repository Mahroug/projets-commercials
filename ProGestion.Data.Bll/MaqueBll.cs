﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProGestion.Data.Layers.Entities;

namespace ProGestion.Data.Bll
{
    public class MarqueBll : SharedBll
    {
        public static bool AddMarque(Marque marque)
        {
            var found = Found(marque);
            if (!found)
            {
                Db.Marques.Local.Add(marque);
            }
            return found;
        }

        private static bool Found(Marque marque)
        {
            return Db.Marques.Local.Any(p => p.Id == marque.Id);
        }
        public static void RemoveCategoryPersonne(Marque marque)
        {
            Db.Marques.Local.Remove(marque);
        }

        public static Marque GetById(int id)
        {
            return Db.Marques.Local.FirstOrDefault(c => c.Id == id);
        }

        public static void UpdateMarque(Marque marque)
        {
            var x = Db.Marques.Local.First(c => c.Id == marque.Id);
            x.Id = marque.Id;
        }
    }
}
