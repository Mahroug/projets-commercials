﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProGestion.Data.Layers.Entities;

namespace ProGestion.Data.Bll
{
    public class DetailsEffetBll : SharedBll
    {
        public static DetailsEffet GetById(int Id)
        {
            return Db.DetailsEffets.Local.FirstOrDefault(p => p.Id == Id);
        }
        public static List <DetailsEffet> GetByIdEffet(int Id)
        {
            return Db.DetailsEffets.Local.Where(p => p.Effet.Id == Id).ToList();
        }
        public static void RemoveDetails(DetailsEffet detailsEffet)
        {
            if (SharedBll.Db.DetailsEffets.Local.Any(o => o.Id == detailsEffet.Id))
            {
                SharedBll.Db.DetailsEffets.Local.Remove(detailsEffet);
            }
        }

    }
}
