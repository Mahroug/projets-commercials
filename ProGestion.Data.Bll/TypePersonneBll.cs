﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProGestion.Data.Layers.Entities;

namespace ProGestion.Data.Bll
{
    public class TypePersonneBll : SharedBll
    {
        public static TypePersonne GetClient()
        {
            return Db.TypePersonnes.Local.SingleOrDefault(us => us.Designation == "client");
        }
        public static TypePersonne GetFournisseur()
        {
            return Db.TypePersonnes.Local.SingleOrDefault(us => us.Designation == "fournisseur");
        }
    }

}
