﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProGestion.Data.Layers.Entities;

namespace ProGestion.Data.Bll
{
    public class RoleBll : SharedBll
    {
        public static void AddRole(Role role)
        {
            Db.Roles.Local.Add(role);
        }
        public static void RemoveRole(Role role)
        {
            Db.Roles.Local.Remove(role);
        }

        public static Role GetById(int id)
        {
            return Db.Roles.Local.FirstOrDefault(c => c.Id == id);
        }

        public static Role GetByName(string name)
        {
            if (name == "") return null;

            return Db.Roles.Local.FirstOrDefault(c => c.Designation == name);
        }

    }
}
