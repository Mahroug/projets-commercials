﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProGestion.Data.Layers.Entities;

namespace ProGestion.Data.Bll
{
    public class FlexyBll : SharedBll
    {
        public static void AddFlexy(Flexy flexy)
        {
           Db.Flexies.Local.Add(flexy);
        }
        public static void RemoveFlexy(Flexy flexy)
        {
            Db.Flexies.Local.Remove(flexy);
        }

       public static Flexy GetById(int id)
        {
            return Db.Flexies.Local.FirstOrDefault(c => c.Id == id);
        }

        public static Flexy GetByNameOperateur(string name)
        {
            if (name == "") return null;

            return Db.Flexies.Local.FirstOrDefault(c => c.Operateur.Designation == name);
        }

        public static void UpdateFlexy(Flexy flexy)
        {
            var x = Db.Flexies.Local.First(c => c.Id == flexy.Id);
            if (flexy.Operateur != null) x.Id = flexy.Id;
        }

    }
}
