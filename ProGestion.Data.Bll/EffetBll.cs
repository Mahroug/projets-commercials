﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProGestion.Data.Layers.Entities;

namespace ProGestion.Data.Bll
{
    public class EffetBll : SharedBll
    {
        public static void AddEffet(Effet effet)
        {
            Db.Effets.Local.Add(effet);
        }

        public static Effet GetById(int id)
        {
            return Db.Effets.Local.FirstOrDefault(c => c.Id == id);
        }

        public static bool RemoveEffet(Effet effet)
        {
            if (effet == null)
                return false;

            // Delete DetailEffet
            if (effet.DetailsEffets != null && effet.DetailsEffets.Any())
            {
                foreach (var item in effet.DetailsEffets.ToList())
                {
                    Db.DetailsEffets.Remove(item);
                }
            }
            Db.Effets.Local.Remove(effet);
            return true;
        }

        public static void UpdateEffet(Effet effet)
        {
            if (Db.Effets == null || effet == null)
                return;
            var o = Db.Effets.Local.First(c => c.Id == effet.Id);
            o.Id = effet.Id;
            
            
        }
        public static void ReglerDette(Effet effet)
        {
            if (Db.Effets == null || effet == null)
                return;
            var o = Db.Effets.Local.First(c => c.Id == effet.Id);
            o.Reste = 0;
        }

    }


}
