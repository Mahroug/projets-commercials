﻿using ProGestion.Data.Layers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProGestion.Data.Layers.Entities;

namespace ProGestion.Data.Bll
{
    public class SharedBll
    {
        public static ProGestionDbContext Db;
        public static string ConnectionString;
        public static string DefaultTicketPrinter;
        public static string DefaultPrinter;
        public static int UcFontSize;
        public static string LocalVersion;
        public static Utilisateur CurrentUser;
        public static string AppPath = AppDomain.CurrentDomain.BaseDirectory;
    }
}
