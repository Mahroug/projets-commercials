﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProGestion.Data.Layers.Entities;

namespace ProGestion.Data.Bll
{
    public class ProduitBll : SharedBll
    {
        public static bool AddProduct(Produit product)
        {
            var found = Found(product);
            if (!found)
            {
                Db.Produits.Local.Add(product);
            }
            return found;
        }

        public static bool Found(Produit product)
        {
            return Db.Produits.Local.Any(p => p.Id == product.Id);
        }

        public static Produit GetById(int id)
        {
            return Db.Produits.Local.FirstOrDefault(c => c.Id == id);
        }

        public static Produit GetByCode(string code)
        {
            return Db.Produits.Local.FirstOrDefault(c => c.Code == code);
        }
       
        public static void UpdateProduct(Produit product)
        {
            var x = Db.Produits.Local.First(c => c.Id == product.Id);
            x.Id = product.Id;
        }

       public static void RemoveProduct(Produit product)
        {
            if (Db.DetailsEffets.Local.Any(o=>product.Lots.Contains(o.Lot)))
            {
                throw new Exception("Impossible supprimer ce produit car il est deja utiliser ");
            }

            Db.Produits.Local.Remove(product);
        }

        public static Produit GetByName(string name)
        {
            return Db.Produits.Local.FirstOrDefault(c => c.Designation.Trim().ToLower() == name.Trim().ToLower());
        }
    }
}
