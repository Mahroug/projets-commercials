﻿using ProGestion.Data.Layers.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProGestion.Data.Bll
{
    public class UtilisateurBll : SharedBll
    {
        public static void AddUser(Utilisateur utilisateur)
        {
            var found = false;
            foreach (var u in Db.Utilisateurs.Local)
                if (utilisateur.Username == u.Username)
                    found = true;
            if (found)
                throw new Exception("User already exist!");
            Db.Utilisateurs.Local.Add(utilisateur);
        }
        public static void UpdateUser(Utilisateur utilisateur)
        {
            Db.Utilisateurs.Local.First(c => c.Id == utilisateur.Id).Username = utilisateur.Username;
            Db.Utilisateurs.Local.First(c => c.Id == utilisateur.Id).Password = utilisateur.Password;
        }

        public static Utilisateur GetAdmin()
        {
            return Db.Utilisateurs.Local.FirstOrDefault(us => us.Role?.Designation == "administrateur");
        }

        public static Utilisateur GetByUsername(string username)
        {
            return Db.Utilisateurs.Local.FirstOrDefault(us => us.Username == username);
        }

        public static bool CheckLogin(Utilisateur user)
        {
            var found = false;
            foreach (var u in Db.Utilisateurs.Local)
                if (u.Username == user.Username && u.Password == user.Password)
                    found = true;
            return found;
        }

        public static void RemoveUser(Utilisateur User)
        {
            Db.Utilisateurs.Local.Remove(User);
        }

    }
}
