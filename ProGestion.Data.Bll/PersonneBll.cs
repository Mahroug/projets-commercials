﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProGestion.Data.Layers.Entities;
using System.Threading.Tasks;

namespace ProGestion.Data.Bll
{
    public class PersonneBll : SharedBll
    {
        public static void AddPersonne(Personne personne)
        {
            try
            {
                if (Found(personne))
                    throw new Exception("Personne déja existe!");
                Db.Personnes.Add(personne);
            }
            catch (Exception e)
            {
               throw new Exception();
            }
        }

        public static bool RemovePersonne(Personne personne)
        {
            try
            {
                if (Db.Flexies.Local.Any(f => f.Personne?.Id == personne.Id) ||
                    Db.Effets.Local.Any(f => f.Personne?.Id == personne.Id)) return false;
                Db.Personnes.Local.Remove(personne);
                return true;
            }
            catch (Exception e)
            {
               throw new Exception();
                return false;
            }
        }
        public static void UpdatePersonne(Personne personne)
        {
            var x = Db.Personnes.First(c => c.Id == personne.Id);
            x.Id = personne.Id;
        }

        public static bool Found(Personne personne)
        {
            return Db.Personnes.Local.Any(b => b.Nom.Trim().ToLower() == personne?.Prenom.Trim().ToLower() && b.Prenom.Trim().ToLower() == personne?.Prenom.Trim().ToLower() && b.Mobile == personne?.Mobile);
        }

        public static Personne GetById(int id)
        {
            return Db.Personnes.Local.FirstOrDefault(w => w.Id == id);
        }
    }
}
