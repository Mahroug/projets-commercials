﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProGestion.Data.Layers.Entities;

namespace ProGestion.Data.Bll
{
    public class LotBll : SharedBll
    {
        public static Lot GetById(int IdLot)
        {
            return Db.Lots.Local.FirstOrDefault(p => p.Id == IdLot);
        }
        public static void AddLot(Lot lot)
        {
            Db.Lots.Add(lot);
        }

        public static void UpdateLot(Lot lot)
        {
            var x = Db.Lots.Local.First(c => c.Id == lot.Id);
            if (lot.Produit != null) x.Id = lot.Id;
        }

        // fournisseur
        public static void AddStockLotFournisseur(List<DetailsEffet> details)
        {
            var xx = Db.Lots.Local.ToList();
            foreach (var detail in details)
            {
                var x = xx.First(c => c.Id == detail.Lot.Id);
                x.PrixAchat = detail.PrixUnitairePaye;
                x.Quantite+= detail.Quantite;
            }
           
        }

        public static void RemoveStockFournisseurLot(List<DetailsEffet> details)
        {
            var xx = Db.Lots.Local.ToList();
            foreach (var detail in details)
            {
                var x = xx.First(c => c.Id == detail.Lot.Id);
                x.Quantite -= detail.Quantite;
            }

        }
        // client
        public static void RemoveStockClientLot(List<DetailsEffet> details)
        {
            var xx = Db.Lots.Local.ToList();
            foreach (var detail in details)
            {
                var x = xx.First(c => c.Id == detail.Lot.Id);
                x.Quantite -= detail.Quantite;
            }

        }

        public static void AddStockClientLot(List<DetailsEffet> details)
        {
            var xx = Db.Lots.Local.ToList();
            foreach (var detail in details)
            {
                var x = xx.First(c => c.Id == detail.Lot.Id);
                x.Quantite += detail.Quantite;
            }

        }
        public static void RemoveLot(Lot lot)
        {
            Db.Lots.Local.Remove(lot);
        }
    }
}
