﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using ProGestion.Data.Layers.Entities;

namespace ProGestion.Data.Bll
{
    public class SettingsBll : SharedBll
    {
        public static Settings GetSetting(string keyName)
        {
            return Db.Settings.Local.FirstOrDefault(pr => pr.KeyName == keyName);
        }

        public static Settings GetSetting(string keyName, Utilisateur utilisateur)
        {
            return Db.Settings.Local.FirstOrDefault(pr => pr.KeyName == keyName && pr.utilisateur.Id == utilisateur.Id);
        }
        public static List<Settings> GetSettinges(Utilisateur utilisateur)
        {
            return Db.Settings.Local.Any() ? Db.Settings.Local.Where(pr => pr.utilisateur.Id == utilisateur.Id).ToList() : null;
        }


        public static Settings Add(string keyName, string keyValue)
        {
            var setting = new Settings() { KeyValue = keyValue, KeyName = keyName };

            Db.Settings.Local.Add(setting);
            return setting;
        }
        public static string GetLicense()
        {
            return Db.Settings.Local.SingleOrDefault(us => us.KeyName == "Lisence").KeyValue;
        }
        //public static void ConfigAppSetting(string key, string value)
        //{
        //    var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
        //    config.AppSettings.Settings[key].Value = value;
        //    config.Save(ConfigurationSaveMode.Modified);
        //    ConfigurationManager.RefreshSection("appSettings");
        //}
    }
}
